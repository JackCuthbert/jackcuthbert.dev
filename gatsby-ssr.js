import * as React from 'react'
import { ThemeProvider } from './src/components/v2/theme'

export const wrapRootElement = ({ element }) => {
  return <ThemeProvider>{element}</ThemeProvider>
}
