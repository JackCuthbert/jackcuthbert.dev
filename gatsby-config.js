const sourceContent = {
  resolve: 'gatsby-source-filesystem',
  options: {
    name: 'markdownContent',
    path: `${__dirname}/content`
  }
}

const transformMarkdown = {
  resolve: 'gatsby-transformer-remark',
  options: {
    plugins: [
      {
        resolve: 'gatsby-remark-prismjs',
        options: {
          noInlineHighlight: true
        }
      },
      {
        resolve: 'gatsby-remark-images',
        options: {
          showCaptions: true,
          backgroundColor: '#FDFDFD',
          maxWidth: 1024
        }
      }
    ]
  }
}

const transformMarkdownLinks = {
  resolve: 'gatsby-plugin-catch-links'
}

const rssFeed = {
  resolve: 'gatsby-plugin-feed',
  options: {
    query: `
      {
        site {
          siteMetadata {
            title
            description
            siteUrl
            site_url: siteUrl
          }
        }
      }
    `,
    feeds: [
      {
        query: `
          {
            allMarkdownRemark(
              sort: { order: DESC, fields: [frontmatter___date] }
              filter: {
                fileAbsolutePath: {
                  regex: "/content/blog/.*.md$/"
                }
              }
            ) {
              edges {
                node {
                  fields { slug }
                  excerpt
                  html
                  frontmatter {
                    title
                    date
                  }
                }
              }
            }
          }
        `,
        serialize: ({ query: { site, allMarkdownRemark } }) => {
          return allMarkdownRemark.edges.map(edge => ({
            ...edge.node.frontmatter,
            description: edge.node.excerpt,
            date: edge.node.frontmatter.date,
            url: site.siteMetadata.siteUrl + edge.node.fields.slug,
            guid: site.siteMetadata.siteUrl + edge.node.fields.slug,
            custom_elements: [{ 'content:encoded': edge.node.html }]
          }))
        },
        output: '/rss.xml',
        title: 'Jack Cuthbert / Software Developer',
        match: '^/blog/'
      }
    ]
  }
}

const buildFavicon = {
  resolve: 'gatsby-plugin-favicon',
  options: {
    logo: './assets/favicon.png'
  }
}

const buildTypography = {
  resolve: 'gatsby-plugin-typography',
  options: {
    pathToConfigModule: 'src/utils/typography'
  }
}

const buildStyledComponents = {
  resolve: 'gatsby-plugin-styled-components',
  options: {
    displayName: true
  }
}

const clickyAnalytics = {
  resolve: `gatsby-plugin-clicky`,
  options: {
    siteId: '101258162'
  }
}

module.exports = {
  siteMetadata: {
    siteUrl: 'https://jackcuthbert.dev',
    title: 'Jack Cuthbert',
    description: 'Software Developer from Melbourne, Australia',
    twitter: '@jckcthbrt'
  },
  plugins: [
    // Development tools
    'gatsby-plugin-typescript',

    // Analytics/tracking
    clickyAnalytics,

    // Content handling
    sourceContent,
    transformMarkdown,
    transformMarkdownLinks,
    rssFeed,

    // Image handling
    buildFavicon,
    'gatsby-transformer-sharp',
    'gatsby-plugin-sharp',

    // Framework/module requirements
    buildTypography,
    buildStyledComponents,
    'gatsby-plugin-react-helmet'
  ]
}
