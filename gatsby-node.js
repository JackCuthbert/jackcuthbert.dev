const path = require('path')
const { createFilePath } = require('gatsby-source-filesystem')

function createQueryForType(type) {
  return `
    query PostsQuery {
      allMarkdownRemark(
        filter: { frontmatter: { type: { eq: "${type}" } } },
        sort: {fields: frontmatter___date, order: ASC}
      ) {
        edges {
          node {
            fields {
              slug
            }
            frontmatter {
              title
            }
          }
        }
      }
    }
  `
}

exports.createPages = async ({ actions, graphql }) => {
  const { createPage } = actions

  const postQuery = await graphql(createQueryForType('Post'))
  const posts = postQuery.data.allMarkdownRemark.edges

  posts.forEach(({ node }, idx) => {
    const prevPost = idx === 0 ? false : posts[idx - 1].node
    const nextPost = idx === posts.length - 1 ? false : posts[idx + 1].node

    const prev = prevPost
      ? { title: prevPost.frontmatter.title, slug: prevPost.fields.slug }
      : undefined

    const next = nextPost
      ? { title: nextPost.frontmatter.title, slug: nextPost.fields.slug }
      : undefined

    createPage({
      path: node.fields.slug,
      component: path.resolve('src/templates/PostTemplate.tsx'),
      context: {
        slug: node.fields.slug,
        prev,
        next
      }
    })
  })

  const pageQuery = await graphql(createQueryForType('Page'))
  const pages = pageQuery.data.allMarkdownRemark.edges

  pages.forEach(({ node }) => {
    createPage({
      path: node.fields.slug,
      component: path.resolve('src/templates/PageTemplate.tsx'),
      context: {
        slug: node.fields.slug
      }
    })
  })

  const noteQuery = await graphql(createQueryForType('Note'))
  const notes = noteQuery.data.allMarkdownRemark.edges

  notes.forEach(({ node }) => {
    createPage({
      path: node.fields.slug,
      component: path.resolve('src/templates/ArticleTemplate.tsx'),
      context: {
        slug: node.fields.slug
      }
    })
  })

  return Promise.resolve()
}

exports.onCreateNode = ({ node, getNode, actions }) => {
  const { createNodeField } = actions

  if (node.internal.type === 'MarkdownRemark') {
    const slug = node.frontmatter.slug
      ? node.frontmatter.slug
      : createFilePath({ node, getNode }).replace(' ', '-').toLowerCase()

    createNodeField({
      node,
      name: 'slug',
      value: slug
    })
  }
}
