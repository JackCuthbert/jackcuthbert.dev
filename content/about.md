---
title: 'About'
type: 'Page'
slug: '/about'
---

# About

Yo! I'm Jack, I'm a full stack software engineer and designer based in
Melbourne, Australia with an eye for detail and a passion for user interface
design. I like to build neat things using shiny new tools when I'm not busy
being awesome at my day job in Melbourne.

## Experience

I currently work full-time as a Software Engineer at [Bueno Systems](http://www.buenosystems.com.au/)
where I write lots of TypeScript and <del>sometimes</del> never break
production. Previously I've worked in both agency and product roles for
[Marketplacer](https://marketplacer.com/) and [Luminary](https://www.luminary.com/).

I prefer to work with Node.js, TypeScript, and/or React but I'm always keen to
learn new things and trying out new ideas. I'm currently looking into Golang,
Kotlin, and Rust to complement my frontend skillset.

I have a background in user interface and web design after attending Griffith
University in Brisbane, Australia and graduating with a Bachelor of Multimedia
majoring in Internet Computing and Internet Marketing.

## Contact & social

I am not currently looking for work, but if you'd like to get in touch you'll
usually be able to find me...

- coding on [GitLab](https://gitlab.com/JackCuthbert) or [GitHub](https://github.com/JackCuthbert)
- lurking on [Twitter](https://twitter.com/jckcthbrt)
- talking with no one on [Mastodon](https://md.jckcthbrt.io/web/accounts/1)
- sharing music to [Last.fm](https://last.fm/user/jckcthbrt)
- trying (and failing) to maintain a [LinkedIn profile](https://www.linkedin.com/in/jackcuthbert/)
- playing games on [Steam](https://steamcommunity.com/id/xs1mple/)
- hanging out on [my Discord server](https://discord.gg/3tX9Ury3Gw)
