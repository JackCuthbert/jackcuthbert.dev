---
title: Getting Started
category: 'Rust'
type: 'Note'
tags: ['backend']
---

# Getting Started

Installing Rust on Arch is a little weird because it doesn't use the officially
supported method.

```bash
sudo pacman -S rustup
rustup toolchain install stable
rustup default stable
```

## Vim integration

Install the [official vim plugin](https://github.com/rust-lang/rust.vim) with:

```vim
Plug 'rust-lang/rust.vim'
```

Then install the [Rust CoC extension](https://github.com/neoclide/coc-rls/)
with:

```
:CocInstall coc-rls
```

## Hello, Rust!

Now that Rust is installed and configured we can actually make something.

```bash
cargo new hello-rust
```

This creates a new folder called `hello-rust` with some files to get started
with. When opening in vim for the first time it may prompt you to install `RLS`.
Accept that and some more files/folders will be created, AFAIK right now it's
safe to add these to `.gitignore` (usually `target/`).

Next you can modify the code in `src/main.rs` or simply run `cargo run` to
compile and run the code.

## Links

- [Official Learn Rust page](https://www.rust-lang.org/learn)
- [The Rust Programming Language (Book)](https://doc.rust-lang.org/book/)
- [Rust by Example](https://doc.rust-lang.org/rust-by-example/)

