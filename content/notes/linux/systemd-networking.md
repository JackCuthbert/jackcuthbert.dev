---
title: Systemd Networking
category: 'Linux'
type: 'Note'
tags: ['ops', 'linux']
---

# Systemd Networking

I've recently stopped using NetworkManger to manage my network and VPN
connections as it seems to be a bit fiddly to get working with WireGuard.

> Make sure all NetworkManager things have been removed.

Install `systemd-resolvconf` if you intend to use WireGuards `wg-quick`
tool.

```bash
sudo pacman -S systemd-resolvconf
```

Then enable `systemd-resolved` and `systemd-networkd`.

```bash
sudo systemctl enable systemd-resolved
sudo systemctl enable systemd-networkd
```

Next remove the file (or symlink) at `/etc/resolv.conf` and replace it with a
new link to the `systemd-resolvd` version.

```bash
sudo rm -f /etc/resolv.conf

# or move it...
# sudo mv /etc/resolv.conf /etc/resolv.conf.bak

sudo ln -sf /run/systemd/resolve/stub-resolv.conf /etc/resolv.conf
```

For pihole users it may be useful to disable any fallback DNS settings. Do this
by creating a file`/etc/systemd/resolved.conf.d/fallback_dns.conf`.

```bash
sudo mkdir -p /etc/systemd/resolved.conf.d
sudo vim /etc/systemd/resolved.conf.d/fallback_dns.conf
```

With this content:

```ini
[Resolve]
FallbackDNS=
```

## Network adapters

I disable [Predictable Network Interface Names](https://www.freedesktop.org/wiki/Software/systemd/PredictableNetworkInterfaceNames/)
because I find it annoying to not know what my devices are called. These
adapters don't change often (if ever) and I'd like the names to be the OG `eth0`
or `wlan0` name. To do this, add `net.ifnames=0` to the kernel options in
`/boot/loader/entries/entry.conf`.

## Wired

Create a file in `/etc/systemd/network/20-wired.network` with the contents:

```ini
[Match]
Name=eth0

[Network]
DHCP=yes
```

## Wireless

Install the `iwd` package and create a file in `/etc/iwd/main.conf` with the content:

```ini
[General]
EnableNetworkConfiguration=true

[Network]
NameResolvingService=systemd
```

Enable/start the service with `sudo systemctl enable --now iwd.service`.

> This service messes about a little bit with devices so a reboot may be
necessary here.

Finally configure the WiFi adapter with `iwctl`.

## References

* [systemd-resolved on Arch Wiki](https://wiki.archlinux.org/index.php/Systemd-resolved)
