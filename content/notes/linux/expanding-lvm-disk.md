---
title: Expanding LVM Disk
category: 'Linux'
type: 'Note'
tags: ['ops', 'backend', 'linux']
---

# Expanding LVM Disk

This guide assumes the following setup. This is the default LVM configuration
after a fresh Debian 10 LVM partition scheme installation with a 32GB disk.

First, add storage to the disk in your virtualisation tool, and reboot.

Next we'll get some information about the disk we're resizing with
`sudo fdisk -l /dev/sda`:

```
Disk /dev/sda: 64 GiB, 68719476736 bytes, 134217728 sectors
Disk model: QEMU HARDDISK
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0x43789823

Device     Boot  Start       End   Sectors  Size Id Type
/dev/sda1  *      2048    499711    497664  243M 83 Linux
/dev/sda2       501758 134217727 133715970 63.8G  5 Extended
/dev/sda5       501760 134217727 133715968 63.8G 8e Linux LVM
```

Using the above obtain the **end sector**, in this case it's N sectors - 1:
`134217728 - 1 = 134217727`.

Then use `parted` to resize the existing partitions:

```shell
sudo parted /dev/sda
```

```
resizepart 2 <end_sector>
resizepart 5 <end_sector>
quit
```

Finally extend the physical and logical volumes and the filesystem using:

1. Extend LVM _physical_ volume: `sudo pvresize /dev/sda5`
1. Get LVM _logical_ volume name: `sudo lvdisplay`
1. Using this name, extend the volume: `sudo lvextend -l +100%FREE <logical volume name (like /dev/vg/root)>` 
1. Resize underlying filesystem: `sudo resize2fs /dev/mapper/<root partition name>`
1. Check parition size with `df -h`
