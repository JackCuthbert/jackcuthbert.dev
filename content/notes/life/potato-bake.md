---
title: Potato Bake
category: 'Life'
type: 'Note'
tags: ['cooking']
---

# Potato Bake


## Ingredients

* 3-4 Potatoes, with or without skin on
* 1/2 cup cream
* 1 small onion
* 2-3 rashers bacon
* Grated cheese

## Instructions

### Potatoes

1. Cut potatoes lengthways in half
1. Turn and slice to desired thickness
1. Place in microwave dish
1. Microwave until tender but not mushy

### Onion and Bacon

1. Dice to desired thickness
1. Fry off in pan with a little oil
1. Once potatoes are done, add the onions and bacon 
