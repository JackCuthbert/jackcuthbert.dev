---
title: Capitalism
category: 'Life'
type: 'Note'
tags: ['minimalism', 'consumerism']
---

# Capitalism

> The self-help minimalists say that keeping expenses low and purchases to a
> minimum can help create a life that is clear and streamlined. This practice
> can also lead to the conclusion that there is not only too much stuff in your
> apartment but too much stuff in the world—that there is, you might say, an
> epidemic of overproduction. If you did say this, you would be quoting Karl
> Marx, who declared that this was the case in 1848, when he and Friedrich
> Engels published “The Communist Manifesto.” Comparing a “society that has
> conjured up such gigantic means of production and of exchange” to “the
> sorcerer who is no longer able to control the powers of the nether world whom
> he has called up by his spells,” they contended that there was “too much means
> of subsistence, too much industry, too much commerce.” Hence, they suggested,
> the boom-and-bust cycle of capitalism, which brings the periodic “destruction
> of a mass of productive forces”—as, perhaps, we experienced in 2008, before
> the rise of Kondo and company.

&ndash; [The Pitfalls and the Potential of the New Minimalism](https://www.newyorker.com/magazine/2020/02/03/the-pitfalls-and-the-potential-of-the-new-minimalism?utm_source=densediscovery&utm_medium=email&utm_campaign=newsletter-issue-87)
