---
title: Ezmode Bolognese
category: 'Life'
type: 'Note'
tags: ['cooking']
---

# Ezmode Bolognese

Wow, my first non-tech/work related section! Here's how I make swaghetti yolonaise.

## Ingredients

* 500g Leggo's Bolognese Pasta Sauce jar
* Handful of spaghetti pasta
* 500g 5-Star Lean Beef Mince

## Instructions

1. Defrost (if frozen) beef mince
2. Break apart mince into heated pan with spatula or similar
3. Cook until all pink/red colour is gone
4. Add spaghetti to pot of boiling water to cook
5. Add full jar of pasta sauce to mince and mix thoroughly
6. Let mince and sauce simmer until desired consistency is reached
7. Strain pasta when cooked
8. Serve

