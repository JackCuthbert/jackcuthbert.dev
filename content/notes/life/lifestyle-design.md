---
title: Lifestyle Design
category: 'Life'
type: 'Note'
tags: ['minimalism']
---

# Lifestyle Design

Right now, a collection of links for suggestions or guides on how to live a more
intentional lifestyle.

## Links

* [How to Create the Life You Want](https://www.youtube.com/watch?v=QG_JWa4_LKQ)
* [Stop being a perfectionist](https://www.youtube.com/watch?v=qbPcHTAgPqY)
