---
title: Clothing
category: 'Life'
type: 'Note'
tags: ['minimalism', 'gear']
---

# Clothing

I've learned through [minimalism](/notes/life/minimalism) that clothing
really doesn't matter all that much. After trying out wearing the same thing
every day for 2 weeks it became apparent that it doesn't matter to my friends or
my colleagues what I wear. No one even noticed unless I pointed it out.

Since then, I've donated, recycled, or thrown out pretty much my entire wardrobe
in an effort to replace everything with things I can wear every day without
thinking about it. I've ended up with a far smaller wardrobe, being more relaxed
in the mornings, and feeling less stressed about laundry.

**I now wear the same thing every day** effectively reducing the number of
decisions that I'm forced to make while still feeling comfortable and retaining
my "metal head" style.

## Everyday

* 10 identical dark grey tshirts from [ascolour](https://www.ascolour.com.au/5065-faded-tee.html?default=FADED%20BLACK).
* 2 identical plain black pairs of super skinny jeans from [JayJays](https://www.jayjays.com.au/shop/en/jayjays/superskinny-denim-jean?colour=BLACK)
* 1 pair of plain black, biker detail super skinny jeans from [ASOS](https://www.asos.com/au/)
* 1 black leather belt from H&M
* 1 pair of [Lakai skate shoes](https://fasttimes.com.au/lakai-riley-hawk-x-indy-charcoal-suede)
* Assorted plain colour socks and underwear

## Sometimes

* Heavy faux leather biker/skater style hooded jacket from [Conner](https://www.connor.com.au/au/),
black+grey of course
* Lighter denim and cotton hooded jacket from H&M in black
and grey
* Light grey beanie from H&M
* Black snapback cap

## Accessories

* The excellent [Incase Reform 13"](https://www.incase.com/reform-backpack-13)
in light grey
* [Seiko SNK795K1](https://watchdirect.com.au/seiko-automatic-snk795-mens-watch.html)
watch with handmade kangaroo leather strap
* All black, stainless steel Citizen J810-S075963 quartz watch
* [Bellroy Hide & Seek](https://bellroy.com/products/hide-and-seek-wallet/leather_rfid_lo/charcoal)
wallet in charcoal
* Limited edition all-black [Orbitkey](https://www.orbitkey.com/) key organiser

