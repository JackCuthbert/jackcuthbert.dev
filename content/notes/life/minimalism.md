---
title: Minimalism
category: 'Life'
type: 'Note'
tags: ['minimalism', 'consumerism']
---

# Minimalism

> Eliminate all that is unnecessary from my life

* Better financial security
* Reduced stress and anxiety levels
* Focus on what's important
* Fewer distractions

## Cleaning and organising

1. Clean _less_, more frequently
2. Create piles of stuff that no longer adds value, remove it all at once when
you're sick of looking at it
3. Create systems for storing essential items in places that make sense
4. Go through draws, cupboards, fridges, etc. 10 mins/week to find things to remove

## Buying stuff

1. Before buying, consider the value the product will add
2. Buy fewer, higher quality things that last longer
3. Avoid eBay, Aliexpress, IKEA, etc
4. Ignore retail employee up-sell, understand the product you're buying
5. [Set aside money each pay cycle](https://www.goodreads.com/book/show/33121747-the-barefoot-investor)
to save for things that add value

## Selling stuff

After many **horrible** experiences trying to sell my stuff on
[Gumtree](http://gumtree.com.au/) I've decided to:

1. Donate stuff instead
2. Give things away for free on Gumtree
3. Gift stuff to friends or family instead
4. Sell stuff to friends or family instead at a _really good_ price*

> *By _really good_ price, I mean practically free. Once I'm selling something
> I've decided that it no longer adds value to my life. Unless I need to recoup
> the cost of the item (which is likely if it was a big purchase) I would prefer
> to help my friends and family out with a great deal.

## Recycling

* [1800 Got Junk](https://www.1800gotjunk.com.au/au_en) will take away anything and aim to dispose of it responsibly
* [Apple GiveBack](https://www.apple.com/au/trade-in/) will recycle old Apple devices for free
* [Appliances Online](https://www.appliancesonline.com.au/) will take away old items for free upon delivery
* [H&M](https://www2.hm.com/en_us/women/campaigns/16r-garment-collecting.html) will accept old clothes to reuse or recycle
* [Harvey Norman](https://www.harveynorman.com.au/customer-service/tv-recycling) will recycle an old TV for you
* [Officeworks](https://www.officeworks.com.au/information/about-us/sustainability/environment/recycling) has a variety of sustainability focused recycling services
* [Redcycle](https://www.redcycle.net.au/where-to-redcycle/) is often present at your local Coles or Woolies (Australian supermarkets) that will recycle your soft plastics

## Digital

1. Scan post/christmas/birthday cards and store them in cloud storage if they
contain sentimental value
2. Scan paper bills/notices/letters that need to be kept for tax or financial
reasons and store them in [secure](https://cryptomator.org/) [cloud](https://nextcloud.com/)
[storage](https://tresorit.com/)
3. Uninstall apps and software from your devices once you've stopped using them
or haven't used them for >1 month
4. Intentionally buy smaller storage devices that force you to consider the
value of the data it holds. [Cheap](https://www.backblaze.com/) [storage](https://aws.amazon.com/s3/) [providers](https://www.crashplan.com/en-us/) exist.
5. Unless you need it for business, delete social media apps and accounts\*
6. Install an ad blocker [in your](https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/) [browser](https://chrome.google.com/webstore/detail/ublock-origin/cjpalhdlnbpafiamejdnhcphjbkeiagm) or [on your network](https://www.youtube.com/watch?v=KBXTnrD_Zs4)

\*This can be controversial but there are a million and one articles about why
this is a good idea and will lead to a happier, healthier, and more productive
life.

## Tech

> Warning: controversial opinion

At most, you'll not need more than 2-4 "smart" devices.

1. Powerful desktop\*
2. Cheap (or employer supplied) laptop ([Lenovo](https://www.lenovo.com/au/en/))\*\*
3. Smart phone
4. Kindle (or similar e-reader) - not an iPad

\*If you don't play games or need the compute power above a mid-range laptop then
you only need #2 and #3. 

\*\*Apple laptops, while amazing in build quality and value-retention, have lost
their spot in my recommendations as they've finally priced themselves out of
what I consider "good value".

## Food

Preparing and managing food isn't something I've been a huge fan but these
services and products have made food preparation, food waste management, and
healthy consumption far more "doable" for me.

1. Meal replacement drinks ([Aussielent](https://aussielent.com.au/))
2. Meal box deliveries ([Dinnerly](https://dinnerly.com.au/), [Hello Fresh](https://www.hellofresh.com.au/), [Marley Spoon](https://marleyspoon.com.au/))

## Reading

Most of these will be documented on my [reading list page](/notes/life/reading-list/)
but the following are specific to minimalism and intentional living.

* [Digital Minimalism: Choosing a Focused Life in a Noisy World](https://www.goodreads.com/book/show/40672036-digital-minimalism)
* [Essentialism: The Disciplined Pursuit of Less](https://www.goodreads.com/book/show/18077875-essentialism)
* [Goodbye, Things: The New Japanese Minimalism](https://www.goodreads.com/book/show/30231806-goodbye-things)
* [The Courage to be Disliked](https://www.goodreads.com/book/show/43306206-the-courage-to-be-disliked)
* [The Life-Changing Magic of Tidying Up](https://www.goodreads.com/book/show/22318578-the-life-changing-magic-of-tidying-up)

## Links

* [The Minimalists Podcast](https://www.theminimalists.com/podcast/)
* [Matt D'Avella: What minimalism really looks like...](https://www.youtube.com/watch?v=jrf_dMnatW0)

