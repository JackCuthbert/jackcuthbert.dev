---
title: Music in 2020
category: 'Life'
type: 'Note'
tags: ['music']
---

# Music in 2020

As part of my 2020 goals, I want to expand the genres of music that I listen to.
This is an ongoing list of what I've enjoyed enough to add to this list.

## January

Artist | Album | Year | Link
-------|-------|------|-----
Arcade Fire | Funeral | 2004 | [Discogs](https://www.discogs.com/Arcade-Fire-Funeral/master/1945)
Beach Bunny | Prom Queen | 2008 | [Discogs](https://www.discogs.com/Beach-Bunny-Prom-Queen/master/1581771)
Clairo | Immunity | 2019 | [Discogs](https://www.discogs.com/Clairo-Immunity/master/1595350)
Girl In Red | Beginnings | 2019 | [Discogs](https://www.discogs.com/Girl-In-Red-Beginnings/master/1603786)
Michael Kiwanuka | Kiwanuka | 2019 | [Discogs](https://www.discogs.com/Michael-Kiwanuka-Kiwanuka/master/1628873)
Miles Davis | Kind of Blue | 1959 | [Discogs](https://www.discogs.com/Miles-Davis-Kind-Of-Blue/master/5460)
Mogwai | Every Country's Sun | 2017 | [Discogs](https://www.discogs.com/Mogwai-Every-Countrys-Sun/master/1230226)
Nat King Cole | This is Nat "King" Cole | 1957 | [Discogs](https://www.discogs.com/Nat-King-Cole-This-Is-Nat-King-Cole/master/372288)

## February

Artist | Album | Year | Link
-------|-------|------|-----
Billie Eillish | Everything I wanted | 2019 | [Discogs](https://www.discogs.com/Billie-Eilish-Everything-I-Wanted/master/1636505)
Billie Eillish | When We All Fall Asleep, Where Do We Go? | 2019 | [Discogs](https://www.discogs.com/Billie-Eilish-When-We-All-Fall-Asleep-Where-Do-We-Go/master/1524311)
My Enemies & I | Sick World | 2015 | [Discogs](https://www.discogs.com/My-Enemies-I-Sick-World/master/1251516)
Rameses B | Cosmonauts | 2020 | [Discogs](https://www.discogs.com/Rameses-B-Cosmonauts/master/1684276)
Tyler, The Creator | Scum Fuck Flower Boy | 2017 | [Discogs](https://www.discogs.com/TylerCreator-Scum-Fuck-Flower-Boy/master/1212609)
Tyler, The Creator | Wolf | 2013 | [Discogs](https://www.discogs.com/TylerCreator-Wolf/master/546979)

## March

Artist | Album | Year | Link
-------|-------|------|-----
Saviour | A Lunar Rose | 2020 | [Discogs](https://www.discogs.com/Saviour-A-Lunar-Rose/release/14858307)
Sleepy Fish | My Room Becomes the Sea | 2019 | [Bandcamp](https://chillhop.bandcamp.com/album/my-room-becomes-the-sea)
Tycho | Simulcast | 2020 | [Discogs](https://www.discogs.com/Tycho-Simulcast/master/1690914)
less.people | Online Mall Music | 2020 | [Soundcloud](https://soundcloud.com/chilledcow/lesspeople-online-mall-music-1)

## April

Artist | Album | Year | Link
-------|-------|------|-----
Various Artists | Jazz For a Rainy Afternoon | 1998 | [Discogs](https://www.discogs.com/Various-Jazz-For-A-Rainy-Afternoon/master/591543)
Pandrezz | Relief | 2020 | [Last.fm](https://www.last.fm/music/PandRezz/Relief)
French 79 | Olympic | 2016 | [Discogs](https://www.discogs.com/French-79-Olympic/master/1094134)

## May

Artist | Album | Year | Link
-------|-------|------|-----
Ghost Data | Cruel Choreography | 2020 | [Last.fm](https://www.last.fm/music/Ghost+Data/Cruel+Choreography)
Ghost Data | Void Walker | 2019 | [Discogs](https://www.discogs.com/Ghost-Data-Void-Walker/release/13952401)
Fraternal Twin | Skin Gets Hot | 2019 | [Discogs](https://www.discogs.com/Fraternal-Twin-Skin-Gets-Hot/master/994200)

## June

Artist | Album | Year | Link
-------|-------|------|-----
DIIV | Deceiver | 2019 | [Discogs](https://www.discogs.com/DIIV-Deceiver/master/1612175)
Dream on, Dreamer | What If I Told You It Doesn't Get Better | 2020 | [Discogs](https://www.discogs.com/Dream-on-Dreamer-What-If-I-Told-You-It-Doesnt-Get-Better/release/15158296)
Snail Mail | Lush | 2018 | [Discogs](https://www.discogs.com/Snail-Mail-Lush/master/1375250)

## July

Artist | Album | Year | Link
-------|-------|------|-----
Blazo | Colors of Jazz | 2011 | [Discogs](https://www.discogs.com/Blazo-Colors-Of-Jazz/master/389931)
City Girl | Goddess of the Hollow | 2020 | [Discogs](https://www.discogs.com/City-Girl-Goddess-Of-The-Hollow/release/15123509)
City Girl | Neon Impasse | 2018 | [Discogs](https://www.discogs.com/City-Girl-Neon-Impasse/master/1654782)
City Girl | Siren of the Formless | 2020 | [Discogs](https://www.discogs.com/City-Girl-Siren-of-the-Formless/release/15122701)
Louie Zong | Pocket | 2020 | [Last.fm](https://www.last.fm/music/Louie+Zong/pocket)
Make Them Suffer | How to Survive a Funeral | 2020 | [Discogs](https://www.discogs.com/Make-Them-Suffer-How-To-Survive-A-Funeral/master/1754386)
Metrik | Ex Machina | 2020 | [Discogs](https://www.discogs.com/Metrik-Ex-Machina/master/1762331)
The Midnight | Monsters | 2020 | [Discogs](https://www.discogs.com/The-Midnight-Monsters/master/1769531)

## August

Artist | Album | Year | Link
-------|-------|------|-----
Alfa Mist | Antiphon | 2017 | [Discogs](https://www.discogs.com/Alfa-Mist-Antiphon/master/1204317)
Dayseeker | Sleeptalk | 2019 | [Discogs](https://www.discogs.com/Dayseeker-Sleeptalk/master/1615240)
Diskette Park | Curve Sequences | 2020 | [Discogs](https://www.discogs.com/Diskette-Park-Curve-Sequences/master/1688307)
Hotel Pools | Still | 2020 | [Last.fm](https://www.last.fm/music/Hotel+Pools/Still)

## September

Artist | Album | Year | Link
-------|-------|------|-----
beabadoobee | Loveworm | 2019 | [Discogs](https://www.discogs.com/beabadoobee-Loveworm/master/1587837)
beabadoobee | Patched Up | 2019 | [Discogs](https://www.discogs.com/beabadoobee-Patched-Up/master/1622571)
Lane 8 | Cross Pollination | 2020 | [Last.fm](https://www.last.fm/music/Lane+8/Cross+Pollination)
Crown the Empire | Sudden Sky | 2019 | [Discogs](https://www.discogs.com/Crown-The-Empire-Sudden-Sky/master/1581604)

&hellip;
