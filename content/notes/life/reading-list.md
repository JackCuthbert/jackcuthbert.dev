---
title: Reading List
category: 'Life'
type: 'Note'
tags: []
---

## Reading list

Recently I've been trying to read more, I've never really been that into reading
novels or self-help books but it's really helped me reduce my overall anxiety.

This is a list of books and visual novels that I've read previously or I'm at
least partway through.

### Science fiction

Possibly my favourite genre, I really enjoy space opera and hard science fiction.

* [Dune](https://www.goodreads.com/book/show/44767458-dune)
* [Leviathan Wakes (The Expanse #1)](https://www.goodreads.com/book/show/8855321-leviathan-wakes)
* [Caliban's War (The Expanse #2)](https://www.goodreads.com/book/show/13276783-caliban-s-war)
* [Abaddon's Gate (The Expanse #3)](https://www.goodreads.com/book/show/16131032-abaddon-s-gate)
* [Cibola Burn (The Expanse #4)](https://www.goodreads.com/book/show/18656030-cibola-burn)
* [Nemesis Games (The Expanse #5)](https://www.goodreads.com/book/show/22886612-nemesis-games)
* [Ready Player One](https://www.goodreads.com/book/show/9969571-ready-player-one)
* [The Martian](https://www.goodreads.com/book/show/18007564-the-martian)
* [Snow Crash](https://www.goodreads.com/book/show/40651883-snow-crash)

### Self-help

Self-help is a new genre I've added to my reading list recently and something
I've always been curious about. Right now I'm focusing on personal finance,
self-esteem, anxiety, and minimalism.

* [Essentialism: The Disciplined Pursuit of Less](https://www.goodreads.com/book/show/18077875-essentialism)
* [Goodbye, Things: The New Japanese Minimalism](https://www.goodreads.com/book/show/30231806-goodbye-things)
* [The Barefoot Investor](https://www.goodreads.com/book/show/33121747-the-barefoot-investor)
* [The Courage to be Disliked](https://www.goodreads.com/book/show/19480382)
* [The Life-Changing Magic of Tidying Up](https://www.goodreads.com/book/show/22318578-the-life-changing-magic-of-tidying-up)

### Other 

* [No Logo](https://www.goodreads.com/book/show/647.No_Logo)

### Visual novels

I've been a fan of comics ever since they were introduced to me by a good friend
of mine while I was at uni. I rarely buy single issues these days but I have a
pretty nice collection of trades.

* [Batman: The Killing Joke](https://www.goodreads.com/book/show/96358.Batman)
* [Blackest Night (Blackest Night #1)](https://www.goodreads.com/book/show/7052524-blackest-night)
* [Captain Marvel, Volume 1: In Pursuit of Flight](https://www.goodreads.com/book/show/15984353-captain-marvel-volume-1)
* [Captain Marvel, Volume 2: Down](https://www.goodreads.com/book/show/17182373-captain-marvel-volume-2)
* [Flashpoint](https://www.goodreads.com/book/show/11501608-flashpoint)
* [Marvel's Ant-Man Prelude](https://www.goodreads.com/book/show/23546911-marvel-s-ant-man-prelude)
* [Saga Vol 1](https://www.goodreads.com/book/show/15704307-saga-vol-1)
* [Saga Vol 2](https://www.goodreads.com/book/show/17131869-saga-vol-2)
* [Saga Vol 3](https://www.goodreads.com/book/show/19358975-saga-vol-3)
* [Saga Vol 4](https://www.goodreads.com/book/show/23093367-saga-vol-4)
* [Saga Vol 5](https://www.goodreads.com/book/show/25451555-saga-vol-5)
* [Saga Vol 6](https://www.goodreads.com/book/show/28862528-saga-vol-6)
* [Saga Vol 7](https://www.goodreads.com/book/show/29237211-saga-vol-7)
* [Y: The Last Man, Vol. 1: Unmanned](https://www.goodreads.com/book/show/156534.Y)
* Countless other Marvel and DC single issues
