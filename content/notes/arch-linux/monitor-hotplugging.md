---
title: Monitor Hotplugging
category: 'Arch Linux'
type: 'Note'
tags: ['linux', 'hardware']
---

# Monitor Hotplugging

[autorandr](https://github.com/phillipberndt/autorandr) is a python script to
manage the automatic detection and application of multiple monitor setups.

## Installation

Single command to install, no systemd service required.

```bash
sudo pacman -S autorandr
```

## Usage

Then create an initial profile, likely for your "mobile" setup. ie. no monitors
connected to a laptop.

```bash
autorandr --save mobile
```

This writes the current configuration to `~/.config/autorandr/mobile/*`.

Next, connect some monitors and set them up manually. For this example I'll set
up the desk I use at work and name it "docked".

```bash
# 1. connect monitors
# 2. apply config with arandr or xrandr
autorandr --save docked
```

Repeat this process for all locations to build up a list of profiles. Now simply
unplugging monitors will trigger autorandr to run its scripts and setup your
monitors.

## Links

* [mons (alternative to autorandr)](https://github.com/Ventto/mons)
* [autorandr on GitHub](https://github.com/phillipberndt/autorandr)
