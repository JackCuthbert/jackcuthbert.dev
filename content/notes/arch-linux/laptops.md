---
title: Laptops
category: 'Arch Linux'
type: 'Note'
tags: ['linux', 'hardware']
---

# Laptops

I haven't had too many laptops to run Arch Linux but of the ones I have used the
following is a few of their quirks and/or their workarounds.

## Dell XPS 13 (9370)

One line review: _Really_ tiny, very pretty tiny screen that's hard to look at,
gets really hot, but supports Linux really well.

* **Booting ArchISO**. Disable secure boot, change RAID to AHCI.
* **Battery life**: Configure [TLP](https://wiki.archlinux.org/index.php/TLP) to
your liking. Everything works.
* **Power drain while in sleep mode**. Add `quiet mem_sleep_default=deep` as a
kernel paremeter.
* **Screen tearing**: [Enable Intel TearFree](https://wiki.archlinux.org/index.php/Intel_graphics#Tearing)
mode in X11.
* **Battery charge thresholds**: All done through newer BIOS version
* **BIOS**: BIOS and firmware updates are fully supported through
[fwupd](https://wiki.archlinux.org/index.php/Fwupd)
* **WiFi**: Atheros adapter, works without issue.

## Lenovo T490

One line review: Bit of a chonker and not much to look at but feels great to use
and supports Linux really well.

* **Booting ArchISO**: Enable Thunderbolt BIOS assist, disable secure boot.
* **Battery life**: Configure [TLP](https://wiki.archlinux.org/index.php/TLP) to
your liking. Everything works.
* **Battery charge thresholds**: Install `tlp`, `tpacpi-bat`, and `acpi_call`
(contrary to what the Arch wiki suggests) and configure in `/etc/tlp.conf`.
* **IRQ Warnings**: Disable TPM module in BIOS. The warnings aren't breaking, just
annoying.
* **Screen tearing**: [Enable Intel TearFree](https://wiki.archlinux.org/index.php/Intel_graphics#Tearing)
mode in X11.
* **BIOS**: BIOS and firmware updates are fully supported through
[fwupd](https://wiki.archlinux.org/index.php/Fwupd)
* **WiFi**: Intel adapter, works without issue. Has some warning output but
doesn't affect functionality or performance.
