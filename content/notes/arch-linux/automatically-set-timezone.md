---
title: Automatically Set Timezone
category: 'Arch Linux'
type: 'Note'
tags: ['linux']
---

# Automatically Set Timezone

Install NetworkManager.

```shell
sudo pacman -S NetworkManager
```

Then ensure that `NetworkManager.service` and `NetworkManager-dispatcher.service`
are both enabled and started.


Create this file at `/etc/NetworkManager/dispatcher.d/09-timezone`:

```shell
#!/bin/sh
case "$2" in
    up)
        timedatectl set-timezone "$(curl --fail https://ipapi.co/timezone)"
    ;;
esac
```

The dispatcher service should now set the timezone automatically.

> I had to whitelist the domain I set in the script (ipapi.co) in my PiHole
> server. Some block lists may prevent this from resolving.

## Links

* [System time on ArchWiki](https://wiki.archlinux.org/index.php/System_time#Setting_based_on_geolocation)

