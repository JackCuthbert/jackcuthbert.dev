---
title: Private Internet Access
category: 'Arch Linux'
type: 'Note'
tags: ['linux', 'privacy']
---

# Private Internet Access

I use PIA for private...  internet... access... and as such, need a simple way to have it integrated into my operating system.

The following will install the OpenVPN profiles in a way that NetworkManager can easily access and connect to them via a system tray icon.

## Prerequisites

This method requires the installation of a few packages:

```bash
sudo pacman -S networkmanager networkmanager-openvpn network-manager-applet 
```

## Installation

```bash
# Download install script
wget -O pia-nm.sh http://www.privateinternetaccess.com/installer/pia-nm.sh

# CHECK CONTENTS!

# Mark script as executable and run it
chmod +x pia-nm.sh
sudo ./pia-nm.sh

# Select UDP and strong encryption

# Restart NetworkManager
sudo systemctl restart NetworkManager.service
```

