---
title: Compressed Memory
category: 'Arch Linux'
type: 'Note'
tags: ['linux']
---

# Compressed Memory

If you're like me and you made the terrible decision of buying a laptop with 8GB
of memory then this is for you. Install zswap.  Heavily inspired (stolen) from
[Peter Roberts article](https://shortestpath.dev/need-more-ram-try-compressing-what-youve-already-got.html).


First, grab the systemd package for using and configuring zswap

```bash
sudo pacman -S systemd-swap
```

Enable 300% compression over the default 200%

```bash
sudo sh -c 'echo -n zswap_zpool=z3fold > /etc/systemd/swap.conf.d/zswap.conf'
```

Enable the service at boot and start it immediately

```bash
sudo systemctl enable --now systemd-swap
```

Check that the settings have applied correctly, [see this article for more](https://shortestpath.dev/need-more-ram-try-compressing-what-youve-already-got.html).

```bash
sudo grep -R . /sys/module/zswap/parameters
```

Check what zswap is doing with your memory

```bash
sudo grep -R . /sys/kernel/debug/zswap
```


