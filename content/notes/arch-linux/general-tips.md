---
title: General Tips
category: 'Arch Linux'
type: 'Note'
tags: ['linux']
---

# General Tips

Arch Linux is my favourite operating system. It's incredibly minimal, fast, and easy to customise. This page documents some tips that don't belong in any specific category. I may split this content out later.

As always, see my [dotfiles](https://gitlab.com/JackCuthbert/dotfiles) for the technically details.

## Key software

* AUR helper: [Aura](https://github.com/aurapm/aura)
* Window manager: [i3-gaps](https://github.com/Airblader/i3)
* Terminal: [kitty](https://github.com/kovidgoyal/kitty)
* Launcher: [rofi](https://github.com/DaveDavenport/rofi)
* Colour theme: [dracula](https://github.com/dracula/dracula-theme)

## Installation

I've written [my own installation guide](./installation/readme.html) based on "[Install Arch Linux on XPS 13](https://gist.github.com/njam/85ab2771b40ccc7ddcef878eb82a0fe9)" by njam. It's also a good idea to follow along with the [official installation guide](https://wiki.archlinux.org/index.php/Installation_Guide) as well.

### Laptop battery life

Use `tlp` for better battery life. The defaults are usually perfectly fine.

```bash
sudo pacman -S tlp                # install tlp
sudo systemctl enable tlp.service # enable the service
```

### HiDPI

As far as I'm aware, HiDPI is a shitshow on Arch if you're not using a mainstream desktop environment like KDE or Gnome. Default DPI everywhere is the best solution for now. If scaling is important it's possible to get "good enough" fractional scaling under Arch, i3, and Xorg with the following.

Set xrandr DPI to 96 with `xrandr --dpi 96` (likely in `.xinitrc`) and set the `GDK_DPI_SCALE` environment variable to a number like `1.25` for 125% scaling (this worked well on my XPS 13 1080p). Firefox should have `layout.css.devPixelsPerPx` configured to `1.25` in `about:config` to scale up the UI by 125% to match the GTK DPI scale.

If you're using an external display where these DPI values are different, try not to cry and unset `GDK_DPI_SCALE`, and the Firefox config, then log out and in.

## User management

It's typically better to use groups to manage user's access to certain features.

### Arduino/ESP8266

Add the current user to the `uucp` group. `gpasswd -a $USER uucp`. [See also](https://wiki.archlinux.org/index.php/Arduino)

### Sudo

Install the `sudo` package.

Instead of adding a specific user to the `/etc/sudoers` file, use the `wheel` group to allow users access to `sudo` command.

1. Use `visudo` and uncomment the `%wheel ALL=(ALL) ALL` line.
2. Add the user with a home folder to the `wheel` group with `useradd -m -G wheel <username>`.

## AppImage

An `.AppImage` file is a completely self-contained executable for Linux, similar to an `.exe` in Windows. Though without installers they're sometimes a bit messy to run.

To install, download the `.AppImage` file to `~/AppImage/` and run the following:

1. `sudo chmod +x ~/AppImage/MyApp.AppImage`
2. `~/AppImage/MyApp.AppImage`

`.desktop` files are created in `~/.local/share/applications` and icons can be found in `~/.local/share/icons/hicolor/*`.

## Links

* [Installation Guide](https://wiki.archlinux.org/index.php/Installation_Guide)
* [Installing Arch Linux on Dell XPS 13](https://gist.github.com/njam/85ab2771b40ccc7ddcef878eb82a0fe9)
* [Official Arch Linux packages](https://www.archlinux.org/packages/)
* [Arch User Repository packages](https://aur.archlinux.org/packages/)

