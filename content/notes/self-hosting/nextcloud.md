---
title: Nextcloud
category: 'Self Hosting'
type: 'Note'
tags: ['ops', 'privacy']
---

# Nextcloud

Using Docker, I run an instance of Nextcloud on my home server that's backed up
to Backblaze B2.

## Docker

This is the bare minimum required to set up Nextcloud with a PostgreSQL
database. Additional setup is required for placing this service behind a reverse
proxy like [nginx-proxy/nginx-proxy](https://github.com/nginx-proxy/nginx-proxy)
or [Traefik](https://docs.traefik.io/routing/providers/docker/).

```yml
version: '3.7'
```

### Network definitions

I use 2 networks, one for public facing services that sit on the `proxy` network
and another for database or storage services that are not available publicly and
sit on the `database` network.

```yml
version: '3.7'

networks:
  proxy:
  database:
```

### Service definitions

My Nextcloud service is designed to use an external USB HDD as the main data
directory and local folders for `html`, `custom_apps`, and `config` directories.

> The permissions of these directories are _really_ important and it took me a
> long time to get this right. I've highlighted the lines that document this.

```yml{10,15}
# ... snip ...

services: 
  nextcloud:
    container_name: 'nextcloud'
    image: nextcloud:19-apache
    restart: unless-stopped
    volumes:

      # Directories here must be owned by "www-data:root"
      - ./nextcloud/www:/var/www/html
      - ./nextcloud/apps:/var/www/html/custom_apps:rw
      - ./nextcloud/config:/var/www/html/config:rw

      # Directories here must be owned by "www-data:www-data"
      - /mnt/usb/nextcloud:/var/www/html/data:rw

    environment:
      VIRTUAL_PORT: 8080
      VIRTUAL_HOST: your.nextcloud.tld
      LETSENCRYPT_HOST: your.nextcloud.tld
      LETSENCRYPT_EMAIL: some@email.com
    depends_on:
      - nginx_proxy
      - postgresql
      - redis
    networks:
      - proxy
      - database

  postgresql:
    image: postgres:12-alpine
    container_name: 'postgresql'
    restart: unless-stopped
    environment:
      NETWORK_ACCESS: internal
    volumes:
      - ./postgresql:/var/lib/postgresql/data
    networks:
      - database

  redis:
    container_name: redis
    image: redis:5-alpine
    restart: unless-stopped
    environment:
      NETWORK_ACCESS: internal
    networks:
      - database
```

## Error 413 (nginx-proxy)

When used in tandem with [nginx-proxy/nginx-proxy](https://github.com/nginx-proxy/nginx-proxy)
and [nginx-proxy/docker-letsencrypt-nginx-proxy-companion](https://github.com/nginx-proxy/docker-letsencrypt-nginx-proxy-companion)
it does not allow for large files to be uploaded out of the box (typically error
413). To resolve this consider the following:

```shell
# open a shell in the proxy container (while it's running)
sudo docker-compose exec proxy_container_name bash

# and edit the vhost for nextcloud
vi /etc/nginx/vhost.d/your.nextcloud.tld
```

Paste the content (updating for your own needs):

```
client_max_body_size        10G;
client_body_buffer_size     400M;
```

Be sure to check that the value has persisted after a `docker-compose restart`,
the companion container may overwrite this file.

> [Per-VIRTUAL_HOST config documentation](https://github.com/nginx-proxy/nginx-proxy#per-virtual_host)
