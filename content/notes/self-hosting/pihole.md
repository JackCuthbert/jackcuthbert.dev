---
title: Pihole
category: 'Self Hosting'
type: 'Note'
tags: ['privacy', 'ops']
---

# Pihole

> A black hole for Internet advertistments.

I use pi-hole for network-wide ad and tracker blocking. [GitHub](https://github.com/pi-hole/pi-hole)

## Full compose file

```yaml
version: '3.8'

services:
  pihole:
    container_name: pihole
    image: pihole/pihole:latest
    restart: always
    ports:
      - '53:53/tcp'
      - '53:53/udp'
      - '8053:80/tcp'
    environment:
      TZ: 'Australia/Melbourne'
      WEBPASSWORD: 'CHANGE_ME'
      DNS1: '1.1.1.1'
      DNS2: '9.9.9.9'
    dns:
      - '127.0.0.1'
      - '1.1.1.1'
    volumes:
      - ./pihole/etc/pihole/:/etc/pihole/
      - ./pihole/etc/dnsmasq.d/:/etc/dnsmasq.d/
    cap_add:
      - NET_ADMIN
```

### Resources

* [The Big Blocklist Collection](https://firebog.net/)

