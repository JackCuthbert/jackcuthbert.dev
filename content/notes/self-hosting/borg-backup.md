---
title: Borg Backup
category: 'Self Hosting'
type: 'Note'
tags: ['ops', 'privacy']
---

# Borg Backup

In order to create offsite backups for my various self-hosted services I use
[BorgBackup](https://borgbackup.readthedocs.io/en/stable/). It is heavily
inspired by their [Automating backups](https://borgbackup.readthedocs.io/en/stable/quickstart.html#automating-backups)
article.


## Setup

Download [this backup script](https://gist.github.com/JackCuthbert/1db41d737ac74f6c5b68d4d4e24bed49)
and place it in `/bin/borg-backup`.

> This script supports adding a `.nobackup` file to a directory to avoid backing
> it up.

First, create the borg repository. This command will prompt you for a passphrase
to encrypt the backup with.

```bash
borg init --encryption=repokey /path/to/repo.borg
```

Create a cronjob with `sudo crontab -e` to run this script at some interval:

```bash
0 17 * * * export BORG_PASSPHRASE=password; borg-backup /path/to/repo.borg /path/to/dir
```

## Backblaze B2

I use the following naming structure for B2 buckets and Borg repositories:

* Borg repository: `service-name.borg`
* Bucket name: `service-name-borg`

Create a script at `/bin/rclone-sync` with the contents:

```bash
#!/bin/sh
rclone sync "/mnt/backup/$1.borg" "backblaze-$1:$1-borg"
```

To sync these Borg repositories to Backblaze B2 I use [Rclone](https://rclone.org/)
in a crontab with the following:

```bash
0 19 * * * export RCLONE_CONFIG_PASS=CONFIG_PASS; rclone-sync service-name
```

This will sync the `service-name.borg` repo to the bucket `service-name-borg`
in B2.

## References

* [Rclone docs](https://rclone.org/docs/)

