---
title: Knowledge Rules
category: 'Meta'
type: 'Note'
tags: []
---

# Knowledge Rules

As I explore documenting everything, I think I'll need to set a few groud-rules for contributing to this project.

1. **Page titles** should be title case
2. **Content headings** should be sentence case
3. **Lists** should never be written as sentences with full stops
4. **Code snippets** should always specify the language
5. **Categories** should follow a consistent pattern
6. **Links** sections should be titled "Links" or "See also" with _level 2_ heading
7. **Category folders** should be capitalised
8. **File name** should be kebab-case

