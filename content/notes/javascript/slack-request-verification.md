---
title: Slack Request Verification
category: 'JavaScript'
type: 'Note'
tags: ['backend', 'javascript', 'slack']
---

# Slack Request Verification

Slack has deprecated the previous method of comparing the verification token to the request's verification token. The below snippet implements the new signing secret method in JavaScript.

```javascript
import { createHmac } from 'crypto'

// pull out the slack request timestamp and signature to compare against
const timestamp = headers['X-Slack-Request-Timestamp']
const actual = headers['X-Slack-Signature']

// use the **raw** body of the request here before middleware parses it (if necessary)
const rawBody = ctx.request.body.raw

// combine the requred fields
const baseString = ['v0', timestamp, rawBody].join(':')

// hash them together
const hash = createHmac('sha256', "YOUR_SLACK_SIGNING_SECRET")
  .update(baseString)
  .digest('hex')

// prepend the version
const computed = `v0=${hash}`

// make the comparison
if (computed !== actual) {
  // failed to verify - exit here
  return
}

// succesfully verified - continue
```

## Links

* [Slack Docs: Verifying requests from Slack](https://api.slack.com/docs/verifying-requests-from-slack)

