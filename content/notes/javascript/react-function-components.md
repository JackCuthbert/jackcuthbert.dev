---
title: React Function Components
category: 'JavaScript'
type: 'Note'
tags: ['frontend', 'javascript', 'typescript', 'react']
---

# React Function Components

Are the hot new awesomeness around town, here are a few things I've learned
about them that weren't immediately clear by reading the documentation.

## Local functions

When creating functions locally (within your component, likely for event
handlers) you should use `useCallback` to ensure that each time something
changes in the component it doesn't pass a _new_ reference of the function into
the handler property. You can think of this as the equivalent to the class
method in a class component.

So this

```tsx{5}
import * as React from 'react'

const MyComponent: React.FC = props => {

  function myHandlerFunc () {
    // do the things!
  }

  return (
    <button onClick={myHandlerFunc}></button>
  )
}
```

becomes this

```tsx{5}
import * as React from 'react'

const MyComponent: React.FC = props => {

  const myHandlerFunc = React.useCallback(() => {
    // do the things!
  }, [])

  return (
    <button onClick={myHandlerFunc}></button>
  )
}

```

