---
title: Debounce Handler
category: 'JavaScript'
type: 'Note'
tags: ['frontend', 'javascript']
---

# Debounce Handler

```html
<!DOCTYPE html>
<html lang="en">
  <head>
      <meta charset="UTF-8">
      <title>Debounce handler demo</title>
  </head>
  <body>
    <input class="my-input" placeholder="Demo" />

    <script>
      let timer;
      let stoppedInterval = 500;
      let inputElement = document.querySelector('.my-input');

      function handleKeyUp() {
        clearTimeout(timer);
        timer = setTimeout(() => {
          console.log(inputElement.value);
        }, stoppedInterval);
      }

      inputElement.addEventListener('keyup', handleKeyUp);
    </script>
  </body>
</html>
```

