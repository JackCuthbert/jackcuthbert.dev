---
title: Docker
category: 'Software'
type: 'Note'
tags: ['ops']
---

# Docker

> By combining its industry-leading container engine technology, an enterprise-grade container platform and world-class services, Docker enables you to bring traditional and cloud native applications built on Windows Server, Linux and mainframe into an automated and secure supply chain, advancing dev to ops collaboration and reducing time to value.

## Compose

Compose is a tool for defining and running multi-container Docker applications. With Compose, you use a YAML file to configure your application’s services. Then, with a single command, you create and start all the services from your configuration. 

[Documentation reference](https://docs.docker.com/compose/)

```bash
#!/bin/bash

# Start containers defined in docker-compose.yml
# -d for detached mode (background)
docker-compose up -d

# Stop any running containers
docker-compose stop

# Stop *and destroy* any running containers
docker-compose down
```

> Note: `docker-compose up` **recreates** the container, including re-injecting environment variables and mounts. `docker-compose start` does not.

## Housekeeping

These are a few commands that are useful in cleaning up a docker installation.

**Delete all stopped containers**

```bash
docker rm $( docker ps -q -f status=exited)
```

**Delete all dangling (unused) images**

```bash
docker rmi $( docker images -q -f dangling=true)
```

**Delete all images**

```bash
docker rmi $(docker images -a -q)
```

**Delete unused volumes**

```bash
docker volume ls -qf dangling=true
```

## Without root

Without detailing the security issues of doing this you can run:

```bash
sudo gpasswd -a $USER docker
```

Optionally add the new group to the session with:

```bash
newgrp docker
```

