---
title: Redshift
category: 'Software'
type: 'Note'
tags: ['productivity', 'linux']
---

# Redshift

[Redshift](https://github.com/jonls/redshift) is a utility to automatically
change the colour temperature of your screen based on your location.

Install redshift and the geolocation ([geoclue](https://gitlab.freedesktop.org/geoclue/geoclue))
service with:

```bash
sudo pacman -S redshift geoclue2
```

## Troubleshooting geoclue

Sometimes the geoclue agent may not start. It _should_ start as a systemd
service, check the status of the service with
`sudo systemctl status geoclue.service`.

**`Failed to query location: Forbidden`** means that the location provider
(Mozilla by default) has rejected the location query. Try hard coding the
location services url to the following in `/etc/geoclue/geoclue.conf`.

```ini
url=https://location.services.mozilla.com/v1/geolocate?key=geoclue
```

Alternatively, try explicitly allowing redshift access to the geoclue agent by
adding the following to `/etc/geoclue/geoclue.conf`

```ini
[redshift]
allowed=true
system=false
users=
```
