---
title: MPD
category: 'Software'
type: 'Note'
tags: ['music', 'productivity']
---

# MPD

Music player deamon.

## Setup

Install the required software:

```bash
sudo aura -S mpd ncmpcpp mpdscribble
sudo aura -Aa mpd-mpris
```

Set up the required files. See my dotfiles for [mpd](https://gitlab.com/JackCuthbert/dotfiles/blob/master/config/mpd.conf)/[ncmpcpp](https://gitlab.com/JackCuthbert/dotfiles/blob/master/config/ncmpcpp.conf) here.

```bash
touch ~/.config/mpd/{database,log,pid,state,sticker.sql}
```

Configure [mpdscribble](https://github.com/MusicPlayerDaemon/mpdscribble/) (~/.mpdscribble/mpdscribble.conf) with your last.fm credentials:

```
username = LAST_FM_USERNAME
password = LAST_FM_PASSWORD
```

Start the services (use `enable` to run them at boot)

```bash
systemctl --user start mpd mpdscribble mpd-mpris
```

## Playerctl

[Playerctl](https://github.com/altdesktop/playerctl) is a CLI controller for a
number of music players. I have some keybindings in i3 to interact with this.

```bash
sudo aura -S playerctl
```

Ensure that the `mpd-mpris.service` is running and check available players with
`playerctl -l`. If anything is returned it should work.

## Links

* [Ncmpcpp on Arch Linux Wiki](https://wiki.archlinux.org/index.php/Ncmpcpp)
* [MPD on Arch Linux Wiki](https://wiki.archlinux.org/index.php/Music_Player_Daemon)

