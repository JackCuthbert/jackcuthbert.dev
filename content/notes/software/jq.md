---
title: Jq
category: 'Software'
type: 'Note'
tags: ['backend', 'tooling']
---

# Jq

[jq](https://github.com/stedolan/jq) is a command line JSON processor, this works really well in tandem with [HTTPie](https://github.com/jakubroztocil/httpie).

## Usage

Get all elements from array response

```bash
jq '.[]'

```

Filter those elements on a key

```bash
# truthy test
jq '.[] | select(.mykey)'

# specific values
jq '.[] | select(.mykey === 'some value')'
```

Return only a specfic key from an array result

```bash
jq '.[].mykey'
```

Return a specific element's value based on a filter on a key in an array response

```bash
jq '.[] | select(.id == 123).mykey'
```

