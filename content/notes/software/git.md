---
title: Git
category: 'Software'
type: 'Note'
tags: ['productivity']
---

# Git

## Branching

### Renaming a Git branch both locally and on the remote host.

Rename branch locally

```shell
git branch -m old_branch new_branch
```

Delete the old branch from the remote

```shell
git push origin :old_branch
```

Push the new branch to the remote

```shell
git push -u origin new_branch
``` 

