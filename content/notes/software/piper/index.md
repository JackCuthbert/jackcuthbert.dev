---
title: Piper
category: 'Software'
type: 'Note'
tags: ['linux']
---

# Piper

Piper is a GTK frontend for managing your mouse firmware via the `ratbagd` DBus
daemon.

![Piper working with my G502](./piper.png)

## Supported devices

It supports an [exstensive list of devices](https://github.com/libratbag/libratbag/tree/master/data/devices)
including my Logitech G502 and Logitech MX Anywhere 2 mice.

## Installation

On Arch it's as simple as `sudo pacman -S piper libratbag`.

On Ubuntu/Pop_OS just run

```bash
sudo add-apt-repository ppa:libratbag-piper/piper-libratbag-git
sudo apt update
sudo apt install piper
```

## Links

* [Libratbag on GitHub](https://github.com/libratbag/libratbag)
* [Piper on GitHub](https://github.com/libratbag/piper)
