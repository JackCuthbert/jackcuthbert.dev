---
title: Neovim
category: 'Software'
type: 'Note'
tags: ['productivity', 'tooling']
---

# Neovim

```bash
sudo pacman -S neovim
```

This page documents some of my preferred setup for neovim.

## Plugins

Like any other vim user, plugins are essential. Here are a few of my must-haves for the projects I work on.

### Autocomplete

[Conquer of Completion](https://github.com/neoclide/coc.nvim) is an intellisense engine for Neovim that works like the VSCode one. Add the following to `init.vim`:

```vim
Plug 'neoclide/coc.nvim', {'branch': 'release'}
```

Additionally, set the following to use `<tab>` to cycle through autocomplete options.

```vim
" Use tab for trigger completion with characters ahead and navigate.
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"
```

### TypeScript & JavaScript

TS and JS require a few different plugins to work well together. Add the following to `init.vim` when using VimPlug:

```vim
Plug 'neoclide/coc.nvim', {'branch': 'release'} " Intellisense engine
Plug 'pangloss/vim-javascript'                  " JavaScript syntax
Plug 'mxw/vim-jsx'                              " JSX syntax
Plug 'HerringtonDarkholme/yats.vim'             " TypeScript syntax
```

Finally, the remaining dependencies:

1. Run `:CocInstall coc-tsserver` ([coc-tsserver](https://github.com/neoclide/coc-tsserver))
2. Run `npm install -g typescript` ([typescript](https://www.typescriptlang.org/))
3. Restart nvim!

[Read more about CoCs language servers](https://github.com/neoclide/coc.nvim/wiki/Language-servers)

### Motion

These are a few quality of life plugins to improve motion within vim

* [vim-surround](https://github.com/tpope/vim-surround) - for easy brackets, quotes, or other surrounding characters
* [auto-pairs](https://github.com/jiangmiao/auto-pairs) - for automatically inserting matching pairs of brackets or similar characters
* [vim-commentary](https://github.com/tpope/vim-commentary) - for easy commenting

### File management

* [nerdtree](https://github.com/scrooloose/nerdtree) - for a sidebar file browser
* [ctrlp](https://github.com/ctrlpvim/ctrlp.vim) - fuzzy file finder
* [vim-auto-mkdir](https://github.com/travisjeffery/vim-auto-mkdir) - automatically create directories when saving files
* [vim-grepper](https://github.com/mhinz/vim-grepper) - search the entire codebase with your favourite grepping tool
  * works well with `ripgrep` (`sudo pacman -S ripgrep`)

## Links

* [My neovim configuration](https://gitlab.com/JackCuthbert/dotfiles/tree/master/config/nvim)

