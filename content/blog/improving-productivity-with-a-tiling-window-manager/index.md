---
title: Improving Productivity with a Tiling Window Manager
date: '2019-02-10'
tags: ['productivity', 'linux']
type: 'Post'
---
Imagine how much time you would save if you never had to take your hands off the keyboard. For the last 2–3 years I've managed to improve my daily productivity, focus, and efficiency using a tiling window manager in macOS and Linux which allows me to do just that.

## Compositing window managers

A window manager, in simpler terms, is the software that controls the placement and appearance of windows in a graphical user interface. For Windows and macOS users this is an out-of-the-box feature that has been around for decades which we all know and love.

![macOS High Sierra](./macos-example.jpg)

Window managers included with mainstream operating systems ([Desktop Window Manager](https://en.wikipedia.org/wiki/Desktop_Window_Manager) on Windows, [Quartz Compositor](https://en.wikipedia.org/wiki/Quartz_Compositor) on macOS, and [GNOME](https://www.gnome.org/) on Ubuntu) are known as compositing window managers. These are generally intuitive to use; they let us move, resize, or hide our work all with the humble mouse and trackpad while being beautiful to look at.

### Mouse non-negotiable

Compositing window managers practically **require** a mouse to use, there is no way to resize or move a window _precisely_ without one (and without 3<sup>rd</sup> party software). Think about all the steps it takes to move and resize a window:

1. Remove your hand from the keyboard
2. Move the mouse to the title bar
3. Click and drag
4. Move the mouse to the edge or corner of the window
5. Click and drag _again_
6. Click back in your window
7. Put your hand back on the keyboard

Look at all those steps! You could be losing anywhere up to 10 seconds of your time for a simple operation that takes 2 or 3 with a tiling system. Repeat that countless times during a full work day and you've lost several minutes for every hour better spent doing something else.

## Tiling window managers

A tiling window manager like [i3](https://i3wm.org/) or [AwesomeWM](https://awesomewm.org/) removes the need to manually control the location, position, and size of your windows. It finds the most predictable and efficient space for a new or existing window while providing incredibly quick ways to manipulate the results, all with the keyboard. I personally use [i3-gaps](https://github.com/Airblader/i3) on my Linux devices and [chunkwm](https://koekeishiya.github.io/chunkwm/) on my Macbook. You can find my configuration for these in my [dotfiles](https://gitlab.com/JackCuthbert/dotfiles/tree/master/config/i3) [repo](https://gitlab.com/JackCuthbert/dotfiles/tree/end-macos-support/config/chunkwm).

> Update: 3 September 2019: I've migrated everything to Linux and no longer maintain macOS configurations. The link above to chunkwm configuration may become out of date eventually.

![My i3gaps desktop](./i3-example.png)

These window managers are often deeply integrated into the operating system (using either [X](https://wiki.archlinux.org/index.php/Xorg)/[Wayland](https://wayland.freedesktop.org/) on Linux and low-level accessibility features in macOS) and take over some common keyboard shortcuts and modifiers. Because of this integration, the operating system remains fast and the window manager shouldn't interfere with existing configuration[^1].

### Keybindings

Typically, a good tiling window manager can be configured to perform very common window and workspace manipulation actions with a few keystrokes that already fit in with your way of working.

> The following examples are some common keybindings that can be fully configured to your own preferences

Changing focus to another window or workspace[^2] is as quick as a `Alt + Left` or `Alt + h` keystroke using arrow keys or Vim-style arrows on the home row. Similarly, a window can be moved to a new location with `Alt + Shift + h` or workspace with the same keybinding and the number of the workspace (`Alt + Shift + 2`).

Windows can be resized by using pre-defined point/pixel increments depending on which manager you use. If you need to get very specific with resizing, a single modifier and a click + drag can make your layouts pixel-perfect[^3].

My personal favourite binding is zooming (or fullscreen) with `Alt + z` or `Alt + f`. This makes a focused window fullscreen and hides other windows from view. It's especially useful when using a laptop screen, as each window behaves as though it's another workspace.

## Closing thoughts

<!-- The most important thing to consider when switching to a tiling window manager is that it will take some time to get used to interacting with your desktop this way. The learning curve for the configuration files can be a little steep but once you've trained up some muscle memory and your configuration is solid you can expect to save a lot of time and frustration in throughout day. -->

This method of interacting with the desktop may not be for everyone. You'll spend a lot of time configuring, learning, and breaking things, before you end up with a workable system you're comfortable with.

In saying that, if you're the kind of person to spend time optimising your workflow and improving your personal productivity then I highly recommend giving a tiling window manager a go.

[^1]: This isn't always the case with macOS, some extra configuration may be required.
[^2]: A workspace is a virtual screen. A single monitor may have many workspaces.
[^3]: I very rarely use this method to resize my windows as I try to avoid using the mouse as much as possible.

### Resources

* [Window manager on Wikipedia](https://en.wikipedia.org/wiki/Window_manager)
* [i3](https://i3wm.org/) and [i3-gaps](https://github.com/Airblader/i3) are tiling window managers for GNU/Linux
* [chunkwm](https://koekeishiya.github.io/chunkwm/) is a tiling window manager for macOS
* [r/UnixPorn](https://reddit.com/r/unixporn) for desktop theming and workflow inspiration
* [DWM](https://dwm.suckless.org/) is a dynamic + tiling window manager for GNU/Linux
* [AwesomeWM](https://awesomewm.org/) is a tiling window manager for GNU/Linux
