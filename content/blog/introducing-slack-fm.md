---
title: Introducing Slack FM
date: '2020-03-11'
tags: ['backend', 'typescript']
type: 'Post'
---

![](https://raw.githubusercontent.com/JackCuthbert/slack-fm/master/header.png)

Slack FM is a small open source side project I spent a few days working on that
syncs your Last.fm now playing music with your Slack status. It's inspired by
the old days where I would sync my now playing music with my MSN Messenger
account.

This post is only a short summary about how it works and how to set it up. For
more information or details about how everything hangs together or all
configurable settings, see [the Slack FM repo on GitHub](https://github.com/JackCuthbert/slack-fm/).

## How it works

Every minute Slack FM will query both your Slack and Last.fm profiles to
determine whether or not your status on Slack should be updated under a few
conditions:

* you are not "away" on Slack
* a custom status hasn't been set on Slack
* something is now playing on Last.fm
* the time is between 8am and 5pm (configurable)
* it's not a weekend (configurable)

## How to set it up

First, you'll need a [Last.fm](https://www.last.fm/) account and part of a Slack
workspace where you're allowed to create Legacy Tokens for your account.

### 1. Scrobble your music to Last.fm

For desktop music players or streaming services Last.fm has [this great page](https://www.last.fm/about/trackmymusic)
detailing some options you have on whatever platform you're using.

For web clients I use [Web Scrobbler](https://web-scrobbler.github.io/) for listening to music in
the browser from services like YouTube Music, YouTube itself, or the web
variants of Spotify or Tidal.

### 2. Run an instance

This part might be the tricky bit for some but the easiest way to run your own
instance of Slack FM is to use Docker. Below is a typical `docker-compose.yml`
file that you might use to run this on your own machine.

```yaml{8-11}
version: '3.7'
services:
  slack_fm:
    image: jckcthbrt/slack-fm:latest
    container_name: slack_fm
    restart: always
    environment:
      TZ: 'Australia/Melbourne'
      SLACK_TOKEN: 'xoxp-123-123-123-123'
      LAST_FM_KEY: 'abcdefghijklmnopqrstuvwxyz123'
      LAST_FM_USERNAME: 'jckcthbrt'
```

Simply swap out the highlighted lines with the details you retrieved above and
run `docker-compose up -d`. This will pull the latest image and start the
service.

## How to contribute

Slack FMs code is hosted in [this GitHub repo](https://github.com/JackCuthbert/slack-fm/)
and is completely free and open source software. Every commit should adhere to
the conventional commits specification to ensure that
[releases](https://github.com/JackCuthbert/slack-fm/releases) are created 
orrectly and subsequently built and pushed to
[DockerHub](https://hub.docker.com/repository/docker/jckcthbrt/slack-fm).

If you have any feature requests or have found a bug in the code, please feel
free to [open an issue](https://github.com/JackCuthbert/slack-fm/issues/new).
