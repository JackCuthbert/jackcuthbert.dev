import React, { FC } from 'react'
import { graphql } from 'gatsby'
import { PageHead } from '../components/PageHead'

import { MappedQuery } from '../types'
import { NavigationV2 } from '../components/v2/Navigation'
import { HeroV2 } from '../components/v2/Hero'
import { Wrap } from '../components/v2/Layout'
import { Writing, Post } from '../components/v2/Writing'
import { NewHomepageNoteV2 } from '../components/v2/NewHomepageNote'
import { ProjectsV2 } from '../components/v2/Projects'
import { FooterV2 } from '../components/v2/Footer'

interface GQLPost {
  fields: {
    slug: string
  }
  timeToRead: number
  excerpt: string
  frontmatter: {
    title: string
    date: string
  }
}

interface QueryResult {
  allMarkdownRemark: {
    edges: Array<{
      node: GQLPost
    }>
  }
}

export const query = graphql`
  query {
    allMarkdownRemark(
      sort: { order: DESC, fields: [frontmatter___date] },
      limit: 4
      filter: {
        fileAbsolutePath: {
          regex: "/(\/content\/blog)/.*\\.md$/"
        }
      }
    ) {
      edges {
        node {
          timeToRead
          fields{
            slug
          }
          excerpt(pruneLength: 140)
          frontmatter {
            date(formatString: "MMMM Do YYYY")
            title
          }
        }
      }
    }
  }
`

function createPost(data: GQLPost): Post {
  return {
    title: data.frontmatter.title,
    date: data.frontmatter.date,
    timeToRead: data.timeToRead,
    slug: data.fields.slug
  }
}

const IndexPage: FC<MappedQuery<QueryResult>> = ({ data }) => {
  const { edges: posts } = data.allMarkdownRemark

  return (
    <Wrap>
      <PageHead description="Software Developer from Melbourne, Australia" />
      <NavigationV2 />
      <HeroV2 />
      <ProjectsV2 />
      <Writing posts={posts.map(p => createPost(p.node))} />
      <NewHomepageNoteV2 />
      <FooterV2 />
    </Wrap>
  )
}

export default IndexPage
