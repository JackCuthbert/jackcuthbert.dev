import React, { FC } from 'react'
import { graphql, Link } from 'gatsby'
import { MappedQuery } from '../types'
import { typography } from '../utils'
import { PageHead } from '../components/PageHead'
import { ArticleContentV2 } from '../components/v2/ArticleContent'
import { NavigationV2 } from '../components/v2/Navigation'
import { Band, Wrap } from '../components/v2/Layout'
import styled, { useThemeContext } from '../components/v2/theme'
import { NewHomepageNoteV2 } from '../components/v2/NewHomepageNote'
import { FooterV2 } from '../components/v2/Footer'

interface QueryResult {
  allMarkdownRemark: {
    group: Array<{
      edges: Array<{
        node: {
          id: string
          fields: {
            slug: string
          }
          frontmatter: {
            title: string
          }
        }
      }>
      fieldValue: string
    }>
  }
}

export const query = graphql`
  query AllKnowledgeQuery {
    allMarkdownRemark(
      filter: { frontmatter: { type: { eq: "Note" } } }
      sort: { order: ASC, fields: frontmatter___title }
    ) {
      group(field: frontmatter___category) {
        edges {
          node {
            id
            frontmatter {
              title
              category
            }
            fields {
              slug
            }
          }
        }
        fieldValue
      }
    }
  }
`

const GrowBand = styled(Band)`
  flex-grow: 1;
`

const Category = styled.div`
  & + & {
    margin-top: ${typography.rhythm(1)};
  }
`

type Props = MappedQuery<QueryResult>

const NotesPage: FC<Props> = props => {
  const { group: groups } = props.data.allMarkdownRemark
  const { theme } = useThemeContext()

  return (
    <Wrap>
      <PageHead
        title="Notes"
        description="These notes are part of an ongoing series where I'm slowly
            putting together my own wiki to document everything I know."
      />
      <NavigationV2 />
      <GrowBand bandColor={theme.background}>
        <ArticleContentV2>
          <h1>Notes</h1>
          <p>
            These notes are part of an ongoing series where I'm slowly putting
            together my own wiki to document everything I know, inspired by{' '}
            <a
              href="https://github.com/RichardLitt/meta-knowledge"
              target="_blank"
              rel="noopener noreferrer"
            >
              other personal knowledge bases on GitHub
            </a>
            .
          </p>
          <hr />
          {groups.map(group => (
            <Category key={group.fieldValue}>
              <h2>{group.fieldValue}</h2>
              <ul>
                {group.edges.map(({ node }) => (
                  <li key={node.id}>
                    <Link to={node.fields.slug}>{node.frontmatter.title}</Link>
                  </li>
                ))}
              </ul>
            </Category>
          ))}
        </ArticleContentV2>
      </GrowBand>
      <NewHomepageNoteV2 />
      <FooterV2 />
    </Wrap>
  )
}

export default NotesPage
