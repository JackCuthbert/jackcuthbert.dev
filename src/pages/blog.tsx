import React, { FC, useMemo } from 'react'
import { graphql, Link as GatsbyLink } from 'gatsby'
import { MappedQuery } from '../types'

import { PageHead } from '../components/PageHead'

import { typography } from '../utils'

import styled, { useThemeContext } from '../components/v2/theme'
import { NavigationV2 } from '../components/v2/Navigation'
import { NewHomepageNoteV2 } from '../components/v2/NewHomepageNote'
import { Wrap, Band, containerStyles } from '../components/v2/Layout'
import { FooterV2 } from '../components/v2/Footer'

interface QueryResult {
  allMarkdownRemark: {
    group: Array<{ fieldValue: string; totalCount: number }>
    edges: Array<{
      node: {
        fields: {
          slug: string
        }
        excerpt: string
        frontmatter: {
          title: string
          date: string
          tags: string[]
        }
      }
    }>
  }
}

export const query = graphql`
  query AllPostsQuery {
    allMarkdownRemark(
      sort: { order: DESC, fields: [frontmatter___date] },
      filter: {
        fileAbsolutePath: {
          regex: "/(\/content\/blog)/.*\\.md$/"
        }
      }
    ) {
      group(field: frontmatter___tags) {
        fieldValue
        totalCount
      }
      edges {
        node {
          fields{
            slug
          }
          excerpt(pruneLength: 70)
          frontmatter {
            title
            tags
            date(formatString: "YYYY-MM-DD")
          }
        }
      }
    }
  }
`

const GrowBand = styled(Band)`
  flex-grow: 1;
`

// TODO: Duped
const Heading = styled.h1`
  color: ${props => props.theme.homePostForeground};
`

// TODO: Duped
const Container = styled.div`
  ${containerStyles}
  padding-top: ${typography.rhythm(2)};
  padding-bottom: ${typography.rhythm(2)};
`

const Post = styled.div`
  display: grid;
  margin-bottom: ${typography.rhythm(1)};

  @media (min-width: 768px) {
    grid-template-columns: 100px auto;
    grid-column-gap: ${typography.rhythm(1)};
  }
`

const PostLink = styled(GatsbyLink)`
  display: inline;
  text-decoration: none;
  color: ${props => props.theme.foreground} !important;

  &:hover {
    color: ${props => props.theme.linkHover} !important;
  }
`

const PostDate = styled.p`
  margin: 0;
  color: ${props => props.theme.foreground};
`

const BlogPage: FC<MappedQuery<QueryResult>> = props => {
  const { edges } = props.data.allMarkdownRemark
  const { theme } = useThemeContext()

  const posts = useMemo(() => {
    return edges.map(({ node }) => ({
      slug: node.fields.slug,
      excerpt: node.excerpt,
      ...node.frontmatter
    }))
  }, [edges])

  return (
    <Wrap>
      <PageHead title="Blog" />
      <NavigationV2 />
      <GrowBand bandColor={theme.homePostBandBackground}>
        <Container>
          <Heading>Blog</Heading>
          {posts.map(post => (
            <Post key={post.slug}>
              <PostDate>
                <small>{post.date}</small>
              </PostDate>
              <div>
                <PostLink to={post.slug}>{post.title}</PostLink>
              </div>
            </Post>
          ))}
        </Container>
      </GrowBand>
      <NewHomepageNoteV2 />
      <FooterV2 />
    </Wrap>
  )
}

export default BlogPage
