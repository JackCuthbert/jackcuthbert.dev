import React, { FC } from 'react'
import { Link } from 'gatsby'

import styled from '../components/v2/theme'
import { PageHead } from '../components/PageHead'
import { NavigationV2 } from '../components/v2/Navigation'
import { Wrap } from '../components/v2/Layout'
import { NewHomepageNoteV2 } from '../components/v2/NewHomepageNote'
import { ArticleContentV2 } from '../components/v2/ArticleContent'

const Heading = styled.h1``

const NotFoundPage: FC = () => {
  return (
    <Wrap>
      <PageHead title="404 Not Found" />
      <NavigationV2 />
      <ArticleContentV2>
        <Heading>404 Not Found</Heading>
        <p>
          That page doesn't exist, maybe it did at some point but it doesn't
          now, <Link to="/">go back home?</Link>
        </p>
        <img src="https://http.cat/404" alt="Not Found" />
      </ArticleContentV2>
      <NewHomepageNoteV2 />
    </Wrap>
  )
}

export default NotFoundPage
