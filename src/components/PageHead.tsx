import React, { FC, useMemo } from 'react'
import { Helmet } from 'react-helmet'
import favicon from '../../assets/favicon.png'

interface Props {
  title?: string
  description?: string
}

export const PageHead: FC<Props> = ({ title, description }) => {
  const pageTitle = useMemo(() => {
    const BASE_TITLE = 'Jack Cuthbert / Software Developer'
    return title !== undefined ? `${BASE_TITLE} – ${title}` : BASE_TITLE
  }, [title])

  const pageDescription = useMemo(() => description ?? '', [description])

  return (
    <Helmet>
      <meta charSet="utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <title>{pageTitle}</title>
      <meta property="og:title" content={pageTitle} />
      <meta property="description" content={pageDescription} />,
      <meta property="og:description" content={pageDescription} />
      <meta property="og:type" content="website" />
      <meta property="og:image" content={favicon} />
    </Helmet>
  )
}
