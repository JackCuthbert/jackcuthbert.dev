import React, { FC } from 'react'
import styled, { useThemeContext } from './theme'
import { Link as GatsbyLink } from 'gatsby'
import { containerStyles, Band } from './Layout'
import typography from '../../utils/typography'

const HeroText = styled.div`
  display: grid;
  grid-template-rows: auto auto;
  grid-row-gap: ${typography.rhythm(1 / 2)};

  max-width: 290px;

  color: ${props => props.theme.heroForeground};

  strong {
    font-weight: 900;
  }

  p {
    margin: 0;
  }

  a {
    color: ${props => props.theme.heroForeground};
  }
`

const Container = styled.div`
  ${containerStyles}
  padding-bottom: ${typography.rhythm(1.5)};
  padding-top: ${typography.rhythm(1.5)};
`

export const Hero: FC = () => {
  const { theme } = useThemeContext()
  return (
    <Band bandColor={theme.heroBackground}>
      <Container>
        <HeroText>
          <p>
            Yo! I'm Jack. I'm a <strong>software developer</strong> from
            Melbourne, Australia.
          </p>
          <p>
            <GatsbyLink to="/about">Need to know more?</GatsbyLink>
          </p>
        </HeroText>
      </Container>
    </Band>
  )
}
export { Hero as HeroV2 }
