import React, { FC } from 'react'
import { css, createGlobalStyle } from 'styled-components'
import { DraculaPrism } from '../../styles/DraculaPrism'
import styled from './theme'
import { typography } from '../../utils'

export const containerStyles = css`
  margin: 0 auto;
  width: 100%;
  padding: 0 20px;
  max-width: 600px;
`

export const Band = styled.div<{ bandColor: string }>`
  background-color: ${props => props.bandColor};
`

const GlobalStyle = createGlobalStyle`
  /* TODO: Find a more elegant solution for this */
  .FOUC_Overlay {
    position: fixed;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    background-color: #FFF;
    pointer-events: none;
    z-index: 1;
  }

  .gatsby-resp-image-figcaption {
    font-size: ${typography.rhythm(0.5)};
    font-style: italic;
  }
`

const WrapStyles = styled.div`
  display: flex;
  flex-direction: column;
  min-height: 100vh;
`

export const Wrap: FC = props => (
  <WrapStyles>
    <GlobalStyle />
    <DraculaPrism />
    {props.children}
  </WrapStyles>
)
