import React, { FC } from 'react'
import { typography } from '../../utils'
import styled, { useThemeContext } from './theme'
import { Band, containerStyles } from './Layout'
import { Link } from 'gatsby'

const Container = styled.div`
  ${containerStyles}
  padding-top: ${typography.rhythm(1)};
`

const Credits = styled.p`
  font-size: 14px;
  color: ${props => props.theme.navigationForeground};

  a,
  a:visited {
    color: ${props => props.theme.navigationForeground};
  }

  a:hover,
  a:focus {
    color: ${props => props.theme.navigationForeground};
  }
`

export const Footer: FC = () => {
  const { theme } = useThemeContext()
  return (
    <Band bandColor={theme.navigationBackground}>
      <Container>
        <Credits>
          Content on <Link to="/blog">blog</Link> pages use the{' '}
          <a
            href="https://creativecommons.org/licenses/by-sa/2.0/"
            rel="noopener noreferrer"
          >
            CC-BY-SA
          </a>{' '}
          license. The{' '}
          <a
            href="https://gitlab.com/JackCuthbert/jackcuthbert.dev"
            target="_blank"
            rel="noopener noreferrer"
          >
            source code
          </a>{' '}
          and <Link to="/notes">notes</Link> use the{' '}
          <a
            target="_blank"
            href="https://opensource.org/licenses/MIT"
            rel="noopener noreferrer"
          >
            MIT
          </a>{' '}
          license. Unsure? Mention me on{' '}
          <a
            target="_blank"
            rel="noopener noreferrer me"
            href="https://md.jckcthbrt.io/@jack"
          >
            Mastodon
          </a>
          .
        </Credits>
      </Container>
    </Band>
  )
}

export { Footer as FooterV2 }
