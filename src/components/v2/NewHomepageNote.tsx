import React, { FC } from 'react'
import styled, { useThemeContext } from './theme'
import { containerStyles, Band } from './Layout'
import { typography } from '../../utils'

const Container = styled.div`
  ${containerStyles}
  padding-top: ${typography.rhythm(1)};
  padding-bottom: ${typography.rhythm(1)};
`

const Text = styled.p`
  color: ${props => props.theme.notificationForeground} !important;

  &:last-of-type {
    margin-bottom: 0;
  }
`

const ThemeToggle = styled.button`
  display: inline;
  background-color: transparent;
  font-weight: 900;
  border: 0;
  margin: 0;
  padding: 0;
  cursor: pointer;
`

export const NewHomepageNote: FC = () => {
  const { theme, toggleTheme } = useThemeContext()
  return (
    <Band bandColor={theme.notificationBackground}>
      <Container>
        <Text>
          This is a preview of a simpler page design that I'm working on over
          the next little bit. I've finally added a{' '}
          <ThemeToggle onClick={toggleTheme}>🎃 dark mode 👻</ThemeToggle>{' '}
          (click it!) but there's still a few pages left to be converted so
          don't worry if things don't look quite right just yet 🙏
        </Text>
      </Container>
    </Band>
  )
}

export { NewHomepageNote as NewHomepageNoteV2 }
