import React, { FC } from 'react'
import styled, { useThemeContext } from './theme'
import { Link as GatsbyLink } from 'gatsby'
import { containerStyles, Band } from './Layout'
import { typography } from '../../utils'

export interface Post {
  slug: string
  title: string
  date: string
  timeToRead: number
}

interface Props {
  posts: Post[]
}

const Container = styled.div`
  ${containerStyles}
  padding-top: ${typography.rhythm(2)};
  padding-bottom: ${typography.rhythm(2)};
`

const Heading = styled.h2`
  color: ${props => props.theme.homePostForeground};
`

const PostContainer = styled(GatsbyLink)`
  display: block;

  padding: ${typography.rhythm(1)};
  margin-bottom: ${typography.rhythm(1)};
  border-radius: 3px;
  border: 1px solid ${props => props.theme.homePostBorder};
  background-color: ${props => props.theme.homePostBackground};

  // Override
  text-decoration: none;

  &:hover {
    border-color: ${props => props.theme.linkHover};
  }
`

const PostTitle = styled.h3`
  font-weight: 400;
  color: ${props => props.theme.homePostForeground};
  margin-bottom: ${typography.rhythm(1 / 2)};
`

const PostDate = styled.p`
  font-size: 14px;
  font-weight: 300;
  color: ${props => props.theme.homePostForeground};
  margin-bottom: 0;
`

const GrowBand = styled(Band)`
  flex-grow: 1;
`

function pluralise(duration: number): string {
  return duration <= 1 ? `${duration} minute` : `${duration} minutes`
}

export const Writing: FC<Props> = props => {
  const { theme } = useThemeContext()

  return (
    <GrowBand bandColor={theme.homePostBandBackground}>
      <Container>
        <Heading>Recent posts</Heading>
        {props.posts.map(post => (
          <PostContainer to={post.slug} key={post.slug}>
            <PostTitle>{post.title}</PostTitle>
            <PostDate>
              {post.date} - {pluralise(post.timeToRead)}
            </PostDate>
          </PostContainer>
        ))}
      </Container>
    </GrowBand>
  )
}
export { Writing as WritingV2 }
