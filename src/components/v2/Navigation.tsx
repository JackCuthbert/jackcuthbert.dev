import React, { FC } from 'react'
import styled, { useThemeContext } from './theme'
import { Link as GatsbyLink } from 'gatsby'
import { containerStyles, Band } from './Layout'

const LogoMark = styled.h1`
  font-size: 16px;
  font-family: 'Muli', sans-serif;
  font-weight: 900;

  color: ${props => props.theme.navigationForeground};

  // Override on typography.ts
  margin: 0;
`

const LogoSVG = styled.svg`
  fill: ${props => props.theme.logoForeground};
  background-color: ${props => props.theme.logoBackground};
  border-radius: 3px;

  max-height: 40px;
  max-width: 40px;
  width: 40px;
  height: 40px;
  padding: 5px;
`

const Logo: FC = () => (
  <LogoSVG viewBox="0 0 512 512" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M89 312.889L232.455 483.556V312.889H89Z" />
    <path d="M256.363 512V113.778L352 3.61083e-06V398.222L256.363 512Z" />
  </LogoSVG>
)

const HomeLink = styled(GatsbyLink)`
  display: grid;
  grid-template-columns: auto auto;
  align-items: inherit; // From "Nav"
  grid-column-gap: 20px;
  text-decoration: none;

  // Optically center against logo
  h1 {
    position: relative;
    top: -2px;
  }
`

const Nav = styled.nav`
  ${containerStyles}
  display: flex;
  justify-content: space-between;
  align-items: center;
  height: 70px;
`

const Links = styled.ul`
  // Override
  margin: 0;
`

const LinkItem = styled.li`
  display: inline;

  & + & {
    margin-left: 20px;
  }
`

const Link = styled(GatsbyLink)`
  margin: 0;
  font-weight: 400;
  color: ${props => props.theme.navigationForeground};
  text-decoration: none;

  &:hover {
    color: ${props => props.theme.linkHover};
  }

  &.hideMobile {
    display: none;

    @media (min-width: 768px) {
      display: inline;
    }
  }
`

export const Navigation: FC = () => {
  const { theme } = useThemeContext()

  return (
    <Band bandColor={theme.navigationBackground}>
      <Nav>
        <HomeLink to="/">
          <Logo />
          <LogoMark>Jack Cuthbert</LogoMark>
        </HomeLink>
        <Links>
          <LinkItem>
            <Link to="/about" className="hideMobile">
              About
            </Link>
          </LinkItem>
          <LinkItem>
            <Link to="/blog">Blog</Link>
          </LinkItem>
          <LinkItem>
            <Link to="/notes" className="hideMobile">
              Notes
            </Link>
          </LinkItem>
        </Links>
      </Nav>
    </Band>
  )
}

export { Navigation as NavigationV2 }
