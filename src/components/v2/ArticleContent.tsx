import React, { FC } from 'react'

import styled, { useThemeContext } from './theme'
import { containerStyles, Band } from './Layout'
import { typography } from '../../utils'

export const Content = styled.div`
  color: ${props => props.theme.homePostForeground};

  h1,
  h2,
  h3,
  a,
  p {
    color: ${props => props.theme.homePostForeground};
  }

  a:hover,
  a:focus {
    color: ${props => props.theme.linkHover};
  }

  li code,
  p code {
    background-color: ${props => props.theme.inlineCodeBackground};
    color: ${props => props.theme.inlineCodeForeground};
  }

  blockquote {
    border-left: 3px solid ${props => props.theme.blockquoteBorder};
    background-color: ${props => props.theme.blockquoteBackground};
  }

  hr {
    background-color: ${props => props.theme.horizontalRule};
  }
`

export const Container = styled.div`
  ${containerStyles}
  padding-top: ${typography.rhythm(2)};
  padding-bottom: ${typography.rhythm(2)};
`

export const ArticleContent: FC = props => {
  const { theme } = useThemeContext()
  return (
    <Band bandColor={theme.homePostBandBackground}>
      <Container>
        <Content>{props.children}</Content>
      </Container>
    </Band>
  )
}

export { ArticleContent as ArticleContentV2 }
