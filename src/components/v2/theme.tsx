import React, {
  FC,
  createContext,
  useCallback,
  useContext,
  useLayoutEffect,
  useState
} from 'react'
import baseStyled, {
  ThemeContext,
  ThemeProvider as StyledThemeProvider,
  ThemedStyledInterface
} from 'styled-components'

const LS_PERSIST_KEY = 'theme_mode'

const lightMode = {
  logoBackground: '#212121',
  logoForeground: '#EAEAEA',

  navigationBackground: '#FFF',
  navigationForeground: '#212121',

  heroBackground: '#556292',
  heroForeground: '#EAEAEA',

  notificationBackground: '#755592',
  notificationForeground: '#FFF',

  homePostBandBackground: '#FDFDFD',
  homePostBackground: '#FFF',
  homePostForeground: '#212121',
  homePostBorder: '#DDD',

  inlineCodeBackground: '#EAD8F9',
  inlineCodeForeground: '#483459',

  blockquoteBorder: '#DDD',
  blockquoteBackground: '#F4F4F4',

  background: '#EAEAEA',
  foreground: '#212121',

  horizontalRule: '#DDD',

  linkHover: '#755592'
}

const darkMode: typeof lightMode = {
  logoBackground: '#EAEAEA',
  logoForeground: '#212121',

  navigationBackground: '#000',
  navigationForeground: '#EAEAEA',

  heroBackground: lightMode.heroBackground,
  heroForeground: lightMode.heroForeground,

  notificationBackground: lightMode.notificationBackground,
  notificationForeground: lightMode.notificationForeground,

  homePostBandBackground: '#212121',
  homePostBackground: '#353535',
  homePostForeground: '#F2F2F2',
  homePostBorder: '#353535',

  inlineCodeBackground: '#433053',
  inlineCodeForeground: '#CD95FF',

  blockquoteBorder: '#656565',
  blockquoteBackground: '#353535',

  background: '#212121',
  foreground: '#EAEAEA',

  horizontalRule: '#5C5C5C',

  linkHover: '#CD95FF'
}

type Theme = typeof lightMode
type Mode = 'dark' | 'light'

type ToggleThemeAction = VoidFunction | undefined
const ToggleThemeContext = createContext<ToggleThemeAction>(undefined)

// TODO: Find a more elegant solution for this
const Overlay = baseStyled.div.attrs({ className: 'FOUC_Overlay' })``

/** Provide and persist to LocalStorage the theme mode */
export const ThemeProvider: FC = ({ children }) => {
  const [mode, setMode] = useState<Mode | undefined>(undefined)
  const [theme, setTheme] = useState(mode === 'light' ? lightMode : darkMode)

  const toggleTheme = useCallback(() => {
    setMode(mode === 'light' ? 'dark' : 'light')
  }, [setMode, mode])

  useLayoutEffect(() => {
    const loaded = localStorage.getItem(LS_PERSIST_KEY) as Mode
    setMode(loaded === null ? 'dark' : loaded)
  }, [setMode])

  useLayoutEffect(() => {
    if (mode === undefined) return

    localStorage.setItem(LS_PERSIST_KEY, mode ?? 'dark')
    setTheme(mode === 'dark' ? darkMode : lightMode)
  }, [mode, setTheme])

  return (
    <StyledThemeProvider theme={theme}>
      <ToggleThemeContext.Provider value={toggleTheme}>
        <>
          {/* TODO: Find a more elegant solution for this */}
          {mode === undefined ? <Overlay /> : null}
          {children}
        </>
      </ToggleThemeContext.Provider>
    </StyledThemeProvider>
  )
}

/** Hook to access either the theme object or the current theme mode */
export function useThemeContext(): { theme: Theme; toggleTheme: VoidFunction } {
  const theme = useContext<Theme>(ThemeContext)
  const toggleTheme = useContext(ToggleThemeContext)

  if (toggleTheme === undefined)
    throw Error('useThemeContext must be used within a ThemeProvider')

  return { theme, toggleTheme }
}

export default baseStyled as ThemedStyledInterface<Theme>
