import React, { FC } from 'react'
import { Link } from 'gatsby'
import styled, { useThemeContext } from '../../components/v2/theme'
import { containerStyles, Band } from '../../components/v2/Layout'
import { typography } from '../../utils'
import slackFmIcon from '../../../assets/slack-fm-favicon.png'
import kaomojiIcon from '../../../assets/kaomoji-favicon.png'

const Container = styled.div`
  ${containerStyles}
  padding-top: ${typography.rhythm(2)};
`

const Heading = styled.h2`
  margin-bottom: ${typography.rhythm(1)};
  color: ${props => props.theme.homePostForeground};
`

const Title = styled.h4`
  display: flex;
  align-items: center;
  font-weight: 600;
  margin-bottom: ${typography.rhythm(1 / 2)};
  color: #eaeaea;

  img {
    width: 20px;
    margin-bottom: 0;
    margin-right: ${typography.rhythm(1 / 2)};
  }
`

const ProjectContainer = styled.div<{ projectColor: string }>`
  background-color: ${props => props.projectColor};
  border-radius: 3px;
  padding: ${typography.rhythm(1)};

  p {
    color: #eaeaea;
    margin-bottom: ${typography.rhythm(1 / 2)};
  }

  p:last-of-type {
    margin-bottom: 0;
  }

  & + & {
    margin-top: ${typography.rhythm(1)};
  }

  a {
    color: #eaeaea;
  }
`

export const Projects: FC = () => {
  const { theme } = useThemeContext()

  return (
    <Band bandColor={theme.homePostBandBackground}>
      <Container>
        <Heading>Projects</Heading>
        <ProjectContainer projectColor="#755592">
          <Title>
            <img src={kaomojiIcon} />
            Kaomoji.moe
          </Title>
          <p>
            A slack app providing instant access to over 10,000 fun and unique
            Japanese kaomoji. V2 is out now and has been built with natural
            language processing, TypeScript, and Google Cloud Run.
          </p>
          <p>
            <a
              href="https://kaomoji.moe/"
              rel="noopener noreferrer"
              target="_blank"
            >
              View project
            </a>
          </p>
        </ProjectContainer>
        <ProjectContainer projectColor="#942B2B">
          <Title>
            <img src={slackFmIcon} />
            slack-fm
          </Title>
          <p>
            A tiny self-hosted service that automatically updates your Slack
            status from your Last.fm profile just like in the good ol' days of
            MSN messenger.
          </p>
          <p>
            <a
              href="https://github.com/JackCuthbert/slack-fm"
              rel="noopener noreferrer"
              target="_blank"
            >
              GitHub
            </a>{' '}
            | <Link to="/blog/introducing-slack-fm/">Blog post</Link>
          </p>
        </ProjectContainer>
      </Container>
    </Band>
  )
}

export { Projects as ProjectsV2 }
