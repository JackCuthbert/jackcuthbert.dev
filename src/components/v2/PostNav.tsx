import React, { FC } from 'react'
import styled, { useThemeContext } from './theme'
import { containerStyles, Band } from './Layout'
import { typography } from '../../utils'
import { Link } from 'gatsby'

const Post = styled(Link)`
  border-radius: 3px;
  border: 1px solid ${props => props.theme.homePostBorder};
  background-color: ${props => props.theme.homePostBackground};
  padding: ${typography.rhythm(1 / 2)};

  // Override
  text-decoration: none;
  color: ${props => props.theme.foreground};

  &:hover {
    cursor: pointer;
    border-color: ${props => props.theme.linkHover};
  }

  p {
    margin: 0;
  }
`

const PostSubtitle = styled.p`
  font-weight: 300;
  font-size: 14px;
`

const Container = styled.div`
  ${containerStyles}
  display: grid;
  grid-template-rows: auto auto;
  grid-row-gap: ${typography.rhythm(1)};
  padding-bottom: ${typography.rhythm(2)};

  // TODO: Temporary to offset the bottom padding on ArticleContentV2
  margin-top: -${typography.rhythm(2)};

  @media (min-width: 475px) {
    grid-template-columns: 1fr 1fr;
    grid-column-gap: 20px;
    grid-row-gap: 0;
  }
`

export interface PostNode {
  title: string
  slug: string
}

interface Props {
  prev?: PostNode
  next?: PostNode
}

const GrowBand = styled(Band)`
  flex-grow: 1;
`

export const PostNav: FC<Props> = props => {
  const { theme } = useThemeContext()

  return (
    <GrowBand bandColor={theme.homePostBandBackground}>
      <Container>
        {props.prev !== undefined ? (
          <Post to={props.prev.slug}>
            <PostSubtitle>Previous post</PostSubtitle>
            <p>{props.prev.title}</p>
          </Post>
        ) : (
          <div />
        )}
        {props.next !== undefined ? (
          <Post to={props.next.slug}>
            <PostSubtitle>Next post</PostSubtitle>
            <p>{props.next.title}</p>
          </Post>
        ) : (
          <div />
        )}
      </Container>
    </GrowBand>
  )
}
