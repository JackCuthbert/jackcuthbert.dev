import Typography from 'typography'

const fontFamily = [
  '-apple-system',
  'BlinkMacSystemFont',
  'Segoe UI',
  'Roboto',
  'Helvetica',
  'Arial',
  'sans-serif',
  'Apple Color Emoji',
  'Segoe UI Emoji',
  'Segoe UI Symbol'
]

const typography = new Typography({
  // Base
  baseFontSize: '16px',
  baseLineHeight: 1.5,
  scaleRatio: 2,

  // Headers
  headerFontFamily: ['Muli', ...fontFamily],
  headerWeight: 900,

  // Body
  bodyFontFamily: ['Muli', ...fontFamily],
  boldWeight: 700,

  // Load in Google Fonts
  googleFonts: [
    { name: 'Muli', styles: ['300', '400', '700', '900'] },
    { name: 'Fira Mono', styles: ['400'] }
  ],

  overrideStyles: ({ rhythm }, _options, _styles) => {
    const codeStyle = {
      fontFamily: '"Fira Mono", monospace',
      borderRadius: '3px',
      lineHeight: '1',
      padding: '2px 5px 2px',
      whiteSpace: 'nowrap'
    }

    return {
      a: {
        transition: 'color 50ms linear',
        fontWeight: 700
      },
      'pre[class*="language-"]': {
        marginTop: 0,
        marginBottom: rhythm(1),
        padding: rhythm(1 / 2)
      },
      li: {
        marginBottom: 0
      },
      'li code': codeStyle,
      'p code': codeStyle,
      'li > p': {
        marginBottom: 0
      },
      'li > ul': {
        marginTop: 0
      },
      pre: {
        borderRadius: '3px'
      },
      blockquote: {
        position: 'relative',
        marginLeft: 0,
        marginRight: 0,
        padding: `${rhythm(1 / 2)} ${rhythm(1)}`,
        borderRadius: '3px'
      },
      '.footnotes p': {
        display: 'inline',
        margin: 0
      }
    }
  }
})

export default typography
