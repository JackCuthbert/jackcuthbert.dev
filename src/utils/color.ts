export const color = {
  /** Purple */
  primary: '#755592',
  /** Blue */
  secondary: '#1F8DC2',
  /** Orange */
  tertiary: '#DA921C',
  /** Background */
  white: '#FDFDFD',
  /** Foreground */
  black: '#212121',
  /** Accented background */
  offWhite: '#F4F4F4',
  /** Accented foreground */
  offBlack: '#4A4A4A',
  /** Border... guh */
  border: '#DDDDDD'
}

/** Convert hex code to RGB values */
function hexToRgb(hex: string): [number, number, number] {
  const [, red, green, blue] = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(
    hex
  )
  return [parseInt(red, 16), parseInt(green, 16), parseInt(blue, 16)]
}

/** Convert a hex code to rgba() string */
export function transparentize(hex: string, value: number): string {
  const [r, g, b] = hexToRgb(hex)
  return `rgba(${String(r)}, ${String(g)}, ${String(b)}, ${String(value)})`
}
