import * as React from 'react'
import { graphql } from 'gatsby'

import { MappedQuery } from '../types'

import { PageHead } from '../components/PageHead'

import { Wrap, Band } from '../components/v2/Layout'
import { NavigationV2 } from '../components/v2/Navigation'
import styled, { useThemeContext } from '../components/v2/theme'
import { Content, Container } from '../components/v2/ArticleContent'
import { NewHomepageNoteV2 } from '../components/v2/NewHomepageNote'
import { FooterV2 } from '../components/v2/Footer'

interface QueryResult {
  markdownRemark: {
    fields: {
      slug: string
    }
    html: string
    excerpt: string
    frontmatter: {
      category: string
      tags: string[]
      title: string
      type: string
      slug: string | null
    }
  }
}

export const query = graphql`
  query KnowledgeQuery($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      fields {
        slug
      }
      html
      excerpt
      frontmatter {
        title
        category
        tags
        type
        slug
      }
    }
  }
`

const GrowBand = styled(Band)`
  flex-grow: 1;
`

type Props = MappedQuery<QueryResult>

const KnowledgeTemplate: React.FC<Props> = props => {
  const { markdownRemark: article } = props.data
  const { theme } = useThemeContext()

  return (
    <Wrap>
      <PageHead
        title={article.frontmatter.title}
        description={article.excerpt}
      />
      <NavigationV2 />

      <GrowBand bandColor={theme.background}>
        <Container>
          <Content>
            <article dangerouslySetInnerHTML={{ __html: article.html }} />

            <hr />

            <h3>Content properties</h3>
            <ul>
              <li>
                <strong>Type:</strong> {article.frontmatter.type}
              </li>
              <li>
                <strong>Category:</strong> {article.frontmatter.category}
              </li>
              <li>
                <strong>Tags:</strong> {article.frontmatter.tags.join(', ')}
              </li>
              <li>
                <strong>Custom slug:</strong>{' '}
                {article.frontmatter.slug !== null
                  ? article.fields.slug
                  : 'None'}
              </li>
            </ul>
          </Content>
        </Container>
      </GrowBand>
      <NewHomepageNoteV2 />
      <FooterV2 />
    </Wrap>
  )
}

export default KnowledgeTemplate
