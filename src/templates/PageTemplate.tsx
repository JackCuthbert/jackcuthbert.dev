import React, { FC } from 'react'
import { graphql } from 'gatsby'

import { MappedQuery } from '../types'
import { PageHead } from '../components/PageHead'

import { Wrap } from '../components/v2/Layout'
import { NavigationV2 } from '../components/v2/Navigation'
import { ArticleContentV2 } from '../components/v2/ArticleContent'
import { NewHomepageNoteV2 } from '../components/v2/NewHomepageNote'
import { FooterV2 } from '../components/v2/Footer'

interface QueryResult {
  markdownRemark: {
    html: string
    excerpt: string
    frontmatter: {
      title: string
    }
  }
}

export const query = graphql`
  query PageQuery($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      html
      excerpt
      frontmatter {
        title
      }
    }
  }
`

type Props = MappedQuery<QueryResult>
const PageTemplate: FC<Props> = ({ data }) => {
  const { markdownRemark: post } = data

  return (
    <Wrap>
      <PageHead title={post.frontmatter.title} description={post.excerpt} />
      <NavigationV2 />
      <ArticleContentV2>
        <div dangerouslySetInnerHTML={{ __html: post.html }} />
      </ArticleContentV2>
      <NewHomepageNoteV2 />
      <FooterV2 />
    </Wrap>
  )
}

export default PageTemplate
