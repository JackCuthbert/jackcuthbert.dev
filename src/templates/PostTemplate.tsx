import * as React from 'react'
import { graphql } from 'gatsby'

import { PageHead } from '../components/PageHead'

import styled from '../components/v2/theme'
import { Wrap } from '../components/v2/Layout'
import { ArticleContentV2 } from '../components/v2/ArticleContent'
import { NavigationV2 } from '../components/v2/Navigation'
import { PostNav, PostNode } from '../components/v2/PostNav'
import { NewHomepageNoteV2 } from '../components/v2/NewHomepageNote'
import { FooterV2 } from '../components/v2/Footer'

interface QueryResult {
  markdownRemark: {
    html: string
    excerpt: string
    timeToRead: number
    frontmatter: {
      title: string
      tags: string[]
      date: string
    }
  }
}

export const query = graphql`
  query PostQuery($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      html
      excerpt
      timeToRead
      frontmatter {
        title
        tags
        date(formatString: "MMMM DD, YYYY")
      }
    }
  }
`

// TODO: Duped
const Heading = styled.h1`
  color: ${props => props.theme.homePostForeground};
`

const Subtitle = styled.p`
  font-style: italic;
  font-weight: 400;
  color: ${props => props.theme.foreground};
`

interface Props {
  data: QueryResult
  pageContext: {
    next?: PostNode
    prev?: PostNode
  }
}

const PostTemplate: React.FC<Props> = ({ data, pageContext }) => {
  const { timeToRead, frontmatter, html, excerpt } = data.markdownRemark

  const readTime = React.useMemo(() => {
    return timeToRead > 1
      ? `${String(timeToRead)} minutes`
      : `${String(timeToRead)} minute`
  }, [timeToRead])

  return (
    <Wrap>
      <PageHead title={frontmatter.title} description={excerpt} />
      <NavigationV2 />
      <ArticleContentV2>
        <Heading>{frontmatter.title}</Heading>
        <Subtitle>
          {frontmatter.date} ― {readTime}
        </Subtitle>
        <article dangerouslySetInnerHTML={{ __html: html }} />
      </ArticleContentV2>
      <PostNav prev={pageContext.prev} next={pageContext.next} />
      <NewHomepageNoteV2 />
      <FooterV2 />
    </Wrap>
  )
}

export default PostTemplate
